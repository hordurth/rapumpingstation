package com.roadauth.pumping.utils;

import com.roadauth.pumping.opcua.data.model.OpcAlarm;

import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;

@SuppressWarnings("rawtypes")
public class AlarmsCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

	@Override
	public TableCell call(TableColumn p) {

		TableCell cell = new TableCell<OpcAlarm, Object>() {

			@Override
			public void updateItem(Object item, boolean empty) {
				super.updateItem(item, empty);

				// logger.debug("AlarmsCellFactory.updateItem");

				setText(empty ? null : getString());
				setGraphic(null);
				TableRow currentRow = getTableRow();
				OpcAlarm currentAlarm = currentRow == null ? null : (OpcAlarm) currentRow.getItem();

				clearRowStyle();
				if (currentAlarm != null) {
					int status = currentAlarm.getStatus();
					// if (!isHover() && !isSelected() && !isFocused()) {
					setRowStyle(status);
					// }
				}
			}

			private void clearRowStyle() {
				ObservableList<String> styleClasses = getStyleClass();
				styleClasses.removeAll("alarmOffNotAcked", "alarmOnNotAcked", "alarmOnAcked");
			}

			private void setRowStyle(int status) {
				ObservableList<String> styleClasses = getStyleClass();
				switch (status) {
				case OpcAlarm.STATUS_OFF_NOTACKED:
					styleClasses.add("alarmOffNotAcked");
					break;

				case OpcAlarm.STATUS_ON_NOTACKED:
					styleClasses.add("alarmOnNotAcked");
					break;

				case OpcAlarm.STATUS_ON_ACKED:
					styleClasses.add("alarmOnAcked");
					break;
				}
			}

			private String getString() {
				return getItem() == null ? "" : getItem().toString();
			}
		};

		return cell;
	}

}