package com.roadauth.pumping.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

@Component
public class RAPumpingUtils {

	private static final Logger logger = LoggerFactory.getLogger(RAPumpingUtils.class);

	@Autowired
	private ApplicationContext applicationContext;

	public Parent loadFxml(Resource fxmlFile) {
		Parent view = null;

		try {
			if (fxmlFile != null) {
				FXMLLoader fxmlLoader;
				fxmlLoader = new FXMLLoader(fxmlFile.getURL());
				fxmlLoader.setControllerFactory(aClass -> applicationContext.getBean(aClass));
				view = fxmlLoader.load();
			}
			else
				throw new Exception();
		}
		catch (Exception ex) {
			if (fxmlFile != null)
				logger.error("Failed to load fxml resource: " + fxmlFile.getFilename(), ex);
			else
				logger.error("loadFxml called with fxmlFile=null!");
		}

		return view;
	}
	
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

}
