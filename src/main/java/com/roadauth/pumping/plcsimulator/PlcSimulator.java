package com.roadauth.pumping.plcsimulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;

import javafx.beans.value.ObservableValue;

@Component
public class PlcSimulator {

	private static final Logger logger = LoggerFactory.getLogger(PlcSimulator.class);

	@Value("${com.roadauth.pumping.debug}")
	private Boolean comRoadauthPumpingDebug;

	@Autowired
	OpcUAService opcService;

	private static final String heatingHotOilStartTag = "Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiHeitolia1_Start_wr";
	private static final String heatingHotOilStopTag = "Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiHeitolia1_Stopp_wr";
	private static final String heatingHotOilRunningTag = "Vegagerdin.Device1.Daelur.HD1_ON";
	private static final String heatingHotOilNotRunningTag = "Vegagerdin.Device1.Daelur.HD1_STOP";

	private static final String heatingPipelineStartTag = "Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiHeitolia2_Start_wr";
	private static final String heatingPipelineStopTag = "Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiHeitolia2_Stopp_wr";
	private static final String heatingPipelineRunningTag = "Vegagerdin.Device1.Daelur.HD2_ON";
	private static final String heatingPipelineNotRunningTag = "Vegagerdin.Device1.Daelur.HD2_STOP";

	private static final String heatingTarTankStartTag = "Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiTank_Start_wr";
	private static final String heatingTarTankStopTag = "Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiTank_Stopp_wr";
	private static final String heatingTarTankIsRunningTag = "Vegagerdin.Device1.StodurTilSkjakerfis.HitiTank_IGangi_rd";
	private static final String heatingTarTankElement1 = "Vegagerdin.Device1.StodurTilSkjakerfis.E1_sk_rd";
	private static final String heatingTarTankElement2 = "Vegagerdin.Device1.StodurTilSkjakerfis.E2_sk_rd";
	private static final String heatingTarTankElement3 = "Vegagerdin.Device1.StodurTilSkjakerfis.E3_sk_rd";
	private static final String heatingTarTankElement4 = "Vegagerdin.Device1.StodurTilSkjakerfis.E4_sk_rd";

	private ObservableValue<Boolean> heatingHotOilTurnOn;
	private ObservableValue<Boolean> heatingHotOilTurnOff;

	private ObservableValue<Boolean> heatingPipelineTurnOn;
	private ObservableValue<Boolean> heatingPipelineTurnOff;

	private ObservableValue<Boolean> heatingTarTankTurnOn;
	private ObservableValue<Boolean> heatingTarTankTurnOff;

	@SuppressWarnings("unchecked")
	public void initSimulator() {
		if (comRoadauthPumpingDebug == true) {
			logger.info("Setting up PLC Simulator");

			// HEATING HOT OIL

			heatingHotOilTurnOn = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION, heatingHotOilStartTag, TAGVALUETYPE.BOOLEAN);
			heatingHotOilTurnOn.addListener((observable, oldValue, newValue) -> {
				if (oldValue != newValue && newValue == true)
					heatingHotOilTurnOn();
			});

			heatingHotOilTurnOff = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION, heatingHotOilStopTag, TAGVALUETYPE.BOOLEAN);
			heatingHotOilTurnOff.addListener((observable, oldValue, newValue) -> {
				if (oldValue != newValue && newValue == true)
					heatingHotOilTurnOff();
			});

			// HEATING PIPELINE

			heatingPipelineTurnOn = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION, heatingPipelineStartTag, TAGVALUETYPE.BOOLEAN);
			heatingPipelineTurnOn.addListener((observable, oldValue, newValue) -> {
				if (oldValue != newValue && newValue == true)
					heatingPipelineTurnOn();
			});

			heatingPipelineTurnOff = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION, heatingPipelineStopTag, TAGVALUETYPE.BOOLEAN);
			heatingPipelineTurnOff.addListener((observable, oldValue, newValue) -> {
				if (oldValue != newValue && newValue == true)
					heatingPipelineTurnOff();
			});

			// HEATING TAR TANK

			heatingTarTankTurnOn = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION, heatingTarTankStartTag, TAGVALUETYPE.BOOLEAN);
			heatingTarTankTurnOn.addListener((observable, oldValue, newValue) -> {
				if (oldValue != newValue && newValue == true)
					heatingTarTankTurnOn();
			});

			heatingTarTankTurnOff = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION, heatingTarTankStopTag, TAGVALUETYPE.BOOLEAN);
			heatingTarTankTurnOff.addListener((observable, oldValue, newValue) -> {
				if (oldValue != newValue && newValue == true)
					heatingTarTankTurnOff();
			});
		}
	}

	/**
	 * Called when the control system sets the start bit for the heating hot oil system to true.
	 * Sets the control system start bit to false, the plc stop bit to false and the plc start bit to true
	 * so the control system knows that the pump has been started.
	 */
	private void heatingHotOilTurnOn() {
		logger.debug("Heating hot oil turning ON");
		opcService.setTagValue(heatingHotOilStartTag, false);
		opcService.setTagValue(heatingHotOilNotRunningTag, false);
		opcService.setTagValue(heatingHotOilRunningTag, true);
	}

	/**
	 * Called when the control system sets the stop bit for the heating hot oil system to true.
	 * Sets the control system stop bit to false, the plc start bit to false and the plc stop bit to true
	 * so the control system knows that the pump has been stopped.
	 */
	private void heatingHotOilTurnOff() {
		logger.debug("Heating hot oil turned OFF");
		opcService.setTagValue(heatingHotOilStopTag, false);
		opcService.setTagValue(heatingHotOilNotRunningTag, true);
		opcService.setTagValue(heatingHotOilRunningTag, false);
	}

	/**
	 * Called when the control system sets the start bit for the heating pipeline system to true.
	 * Sets the control system start bit to false, the plc stop bit to false and the plc start bit to true
	 * so the control system knows that the pump has been started.
	 */
	private void heatingPipelineTurnOn() {
		logger.debug("Heating pipeline turning ON");
		opcService.setTagValue(heatingPipelineStartTag, false);
		opcService.setTagValue(heatingPipelineNotRunningTag, false);
		opcService.setTagValue(heatingPipelineRunningTag, true);
	}

	/**
	 * Called when the control system sets the stop bit for the heating pipeline system to true.
	 * Sets the control system stop bit to false, the plc start bit to false and the plc stop bit to true
	 * so the control system knows that the pump has been stopped.
	 */
	private void heatingPipelineTurnOff() {
		logger.debug("Heating pipeline turned OFF");
		opcService.setTagValue(heatingPipelineStopTag, false);
		opcService.setTagValue(heatingPipelineNotRunningTag, true);
		opcService.setTagValue(heatingPipelineRunningTag, false);
	}

	/**
	 * Called when the control system sets the start bit for the heating tar tank system to true.
	 * Sets the control system start bit to false and then turns on all 4 heating elements and the isRunning tag.
	 */
	private void heatingTarTankTurnOn() {
		logger.debug("Heating tar tank turning ON");
		opcService.setTagValue(heatingTarTankStartTag, false);
		opcService.setTagValue(heatingTarTankElement1, true);
		opcService.setTagValue(heatingTarTankElement2, true);
		opcService.setTagValue(heatingTarTankElement3, true);
		opcService.setTagValue(heatingTarTankElement4, true);
		opcService.setTagValue(heatingTarTankIsRunningTag, true);

	}

	/**
	 * Called when the control system sets the stop bit for the heating tar tank system to true.
	 * Sets the control system start bit to false and then turns off all 4 heating elements and the isRunning tag.
	 */
	private void heatingTarTankTurnOff() {
		logger.debug("Heating tar tank turned OFF");
		opcService.setTagValue(heatingTarTankStopTag, false);
		opcService.setTagValue(heatingTarTankElement1, false);
		opcService.setTagValue(heatingTarTankElement2, false);
		opcService.setTagValue(heatingTarTankElement3, false);
		opcService.setTagValue(heatingTarTankElement4, false);
		opcService.setTagValue(heatingTarTankIsRunningTag, false);
	}

}
