package com.roadauth.pumping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ConfigurableApplicationContext;

import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.UAServer;
import com.roadauth.pumping.plcsimulator.PlcSimulator;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class RAPumpingFXApplication extends Application {

	private static Logger logger = LoggerFactory.getLogger(RAPumpingFXApplication.class);

	private ConfigurableApplicationContext applicationContext;

	@Override
	public void init() {
		applicationContext = new SpringApplicationBuilder(RAPumpingSBApplication.class).run();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			// Initialise the OPCUA service, connect to KepServer
			OpcUAService opcService = applicationContext.getBean(OpcUAService.class);
			opcService.connectToUAServers();
			UAServer uaServer = opcService.getServerByName("kepware");
			if (uaServer != null && uaServer.serverIsOk()) {
				// Notify app we are ready, activates the StageInitializer which opens the main
				// view
				applicationContext.publishEvent(new StageReadyEvent(primaryStage));
			}
			else {
				// Not connected to Kepserver, notify user and exit
				logger.error("Error connecting to OPCUA server");
				Alert dlg = new Alert(AlertType.ERROR, "Gat ekki tengst Kepware OPCUA server!", ButtonType.CLOSE);
				dlg.showAndWait();
				Platform.exit();
			}
			
			// Start the PlcSimulator (starts only if debug is true in application.properties)
			// This needs to happen AFTER OPCUA service is up (happens above)
			PlcSimulator simulator = applicationContext.getBean(PlcSimulator.class);
			simulator.initSimulator();
		}
		catch (Exception ex) {
			logger.error("Error during startup!", ex);
		}
	}

	@SuppressWarnings("serial")
	static class StageReadyEvent extends ApplicationEvent {
		public StageReadyEvent(Stage stage) {
			super(stage);
		}

		public Stage getStage() {
			return ((Stage) getSource());
		}
	}

	@Override
	public void stop() {
		OpcUAService opcService = applicationContext.getBean(OpcUAService.class);

		// Disconnect from KepServer
		opcService.disconnectFromUAServers();

		applicationContext.close();
		Platform.exit();
	}

}
