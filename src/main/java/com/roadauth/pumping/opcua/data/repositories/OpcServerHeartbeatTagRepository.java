package com.roadauth.pumping.opcua.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.opcua.data.model.OpcServerHeartbeatTag;

@Repository
public interface OpcServerHeartbeatTagRepository extends JpaRepository<OpcServerHeartbeatTag, Integer> {

	@Query("SELECT tag FROM OpcServerHeartbeatTag tag WHERE tag.enabled = 1")
	List<OpcServerHeartbeatTag> findAllEnabled();

}
