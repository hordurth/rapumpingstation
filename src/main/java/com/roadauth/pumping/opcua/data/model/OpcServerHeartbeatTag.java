package com.roadauth.pumping.opcua.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.prosysopc.ua.stack.builtintypes.DataValue;

import javafx.beans.property.SimpleObjectProperty;

@Entity
@Table(name = "serverheartbeattag")
public class OpcServerHeartbeatTag {

	@Column(name = "serverheartbeattag_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@Column(name = "server_id")
	private Integer serverId;

	@Column(name = "tag", length = 400)
	private String tag;

	@Column(name = "namespaceid")
	private Integer namespaceId;

	@Column(name = "enabled")
	private Integer enabled;

	@Transient
	private SimpleObjectProperty<DataValue> lastValue = new SimpleObjectProperty<DataValue>(null);

	public Integer getId() {
		return id;
	}

	public Integer getServerId() {
		return serverId;
	}

	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Integer getNamespaceId() {
		return namespaceId;
	}

	public void setNamespaceId(Integer namespaceId) {
		this.namespaceId = namespaceId;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public DataValue getLastValue() {
		return lastValue.get();
	}

	public void setLastValue(DataValue lastValue) {
		this.lastValue.set(lastValue);
	}

}
