package com.roadauth.pumping.opcua.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "subscriptiontag")
public class OpcSubscriptionTag {

	@Column(name = "subscriptiontag_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@ManyToOne(targetEntity = OpcSubscription.class, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "subscription_id")
	private OpcSubscription subscription;

	@Column(name = "tagname", length = 200)
	private String tagName;

	@Column(name = "alarmenabled")
	private Integer alarmEnabled;

	@Column(name = "alarmmessage", length = 200)
	private String alarmMessage;

	public Integer getId() {
		return id;
	}

	public OpcSubscription getSubscription() {
		return subscription;
	}

	public void setSubscription(OpcSubscription subscription) {
		this.subscription = subscription;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public Integer getAlarmEnabled() {
		return alarmEnabled;
	}

	public void setAlarmEnabled(Integer alarmEnabled) {
		this.alarmEnabled = alarmEnabled;
	}

	public String getAlarmMessage() {
		return alarmMessage;
	}

	public void setAlarmMessage(String alarmMessage) {
		this.alarmMessage = alarmMessage;
	}

}
