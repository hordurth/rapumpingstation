package com.roadauth.pumping.opcua.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.opcua.data.model.OpcSubscription;
import com.roadauth.pumping.opcua.data.model.OpcSubscriptionTag;

@Repository
public interface OpcSubscriptionTagRepository extends JpaRepository<OpcSubscriptionTag, Integer> {

	@Query("SELECT tag FROM OpcSubscriptionTag tag WHERE tag.subscription = (:subscription)")
	List<OpcSubscriptionTag> findAllForSubscription(@Param("subscription") OpcSubscription subscription);

}
