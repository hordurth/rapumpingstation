package com.roadauth.pumping.opcua.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscription")
public class OpcSubscription {

	@Column(name = "subscription_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@Column(name = "server_id")
	private Integer serverId;

	@Column(name = "subscriptionname", length = 50)
	private String subscriptionName;

	@Column(name = "namespaceid")
	private Integer namespaceId;

	@Column(name = "description", length = 500)
	private String description;

	@Column(name = "heartbeatsubscriptiontag_id")
	private Integer heartbeatSubscriptionTagId;

	@Column(name = "logfrequency")
	private Integer logFrequency;

	public Integer getId() {
		return id;
	}

	public Integer getServerId() {
		return serverId;
	}

	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}

	public String getSubscriptionName() {
		return subscriptionName;
	}

	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}

	public Integer getNamespaceId() {
		return namespaceId;
	}

	public void setNamespaceId(Integer namespaceId) {
		this.namespaceId = namespaceId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHeartbeatSubscriptionTagId() {
		return heartbeatSubscriptionTagId;
	}

	public void setHeartbeatSubscriptionTagId(Integer heartbeatSubscriptionTagId) {
		this.heartbeatSubscriptionTagId = heartbeatSubscriptionTagId;
	}

	public Integer getLogFrequency() {
		return logFrequency;
	}

	public void setLogFrequency(Integer logFrequency) {
		this.logFrequency = logFrequency;
	}

}
