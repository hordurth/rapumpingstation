package com.roadauth.pumping.opcua.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.opcua.data.model.OpcAlarm;

@Repository
public interface OpcAlarmRepository extends JpaRepository<OpcAlarm, Integer> {

	@Query("SELECT alarm FROM OpcAlarm alarm WHERE alarm.status > 1")
	List<OpcAlarm> findAllActive();

}
