package com.roadauth.pumping.opcua.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.opcua.data.model.OpcTagGroup;

@Repository
public interface OpcTagGroupRepository extends JpaRepository<OpcTagGroup, Integer> {

}
