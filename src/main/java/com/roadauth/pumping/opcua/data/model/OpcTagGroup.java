package com.roadauth.pumping.opcua.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taggroup")
public class OpcTagGroup {

	@Column(name = "taggroup_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@Column(name = "groupname", length = 50)
	private String groupName;

	@Column(name = "namespaceid")
	private Integer nameSpaceId;

	@Column(name = "tagnameprefix", length = 200)
	private String tagNamePrefix;

	@Column(name = "server_id")
	private Integer serverId;

	public Integer getId() {
		return id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getNameSpaceId() {
		return nameSpaceId;
	}

	public void setNameSpaceId(Integer nameSpaceId) {
		this.nameSpaceId = nameSpaceId;
	}

	public String getTagNamePrefix() {
		return tagNamePrefix;
	}

	public void setTagNamePrefix(String tagNamePrefix) {
		this.tagNamePrefix = tagNamePrefix;
	}

	public Integer getServerId() {
		return serverId;
	}

	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}

	public String getFullTagName(String tagName) {
		return this.tagNamePrefix + "." + tagName;
	}

}
