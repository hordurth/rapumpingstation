package com.roadauth.pumping.opcua.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.opcua.data.model.OpcSubscription;

@Repository
public interface OpcSubscriptionRepository extends JpaRepository<OpcSubscription, Integer> {

}
