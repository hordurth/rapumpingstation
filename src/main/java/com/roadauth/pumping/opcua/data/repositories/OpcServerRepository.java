package com.roadauth.pumping.opcua.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.opcua.data.model.OpcServer;

@Repository
public interface OpcServerRepository extends JpaRepository<OpcServer, Integer> {

	@Query("SELECT server FROM OpcServer server WHERE server.enabled = 1")
	List<OpcServer> findAllEnabled();

}
