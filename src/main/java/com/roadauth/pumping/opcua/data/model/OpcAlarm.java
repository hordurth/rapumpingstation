package com.roadauth.pumping.opcua.data.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.Observable;
import javafx.beans.property.SimpleIntegerProperty;

@Entity
@Table(name = "alarms")
public class OpcAlarm {

	public static final int STATUS_OFF = 0;
	public static final int STATUS_OFF_NOTACKED = 1;
	public static final int STATUS_ON_ACKED = 2;
	public static final int STATUS_ON_NOTACKED = 3;

	private static Logger logger = LoggerFactory.getLogger(OpcAlarm.class);

	private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("kk:mm:ss");

	@Column(name = "alarm_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@ManyToOne(targetEntity = OpcSubscriptionTag.class, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "subscriptiontag_id")
	private OpcSubscriptionTag subscriptionTag;

	@Column(name = "status")
	private Integer status;

	@Column(name = "timeon", columnDefinition = "TIMESTAMP")
	private LocalDateTime timeOn;

	@Column(name = "valueon", length = 50)
	private String valueOn;

	@Column(name = "timeack", columnDefinition = "TIMESTAMP")
	private LocalDateTime timeAck;

	@Column(name = "valueack", length = 50)
	private String valueAck;

	@Column(name = "timeoff", columnDefinition = "TIMESTAMP")
	private LocalDateTime timeOff;

	@Column(name = "valueoff", length = 50)
	private String valueOff;

	@Transient
	private SimpleIntegerProperty statusProperty;

	@Transient
	private Observable[] observableProperties;

	public OpcAlarm() {
		statusProperty = new SimpleIntegerProperty(0);
		observableProperties = new Observable[] { statusProperty };
	}

	@PostLoad
	public void postLoadInit() {
		int stat = (getStatus() != null ? getStatus() : 0);
		statusProperty.set(stat);
	}

	public Integer getId() {
		return id;
	}

	public OpcSubscriptionTag getSubscriptionTag() {
		return subscriptionTag;
	}

	public void setSubscriptionTag(OpcSubscriptionTag subscriptionTag) {
		this.subscriptionTag = subscriptionTag;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		if (logger.isDebugEnabled())
			logger.debug("Changing tag: " + getSubscriptionTagName() + " status from: " + getStatus() + " to: " + status);
		this.status = status;
		statusProperty.set(status);
	}

	public SimpleIntegerProperty getStatusProperty() {
		if (statusProperty == null)
			statusProperty = new SimpleIntegerProperty(getStatus());
		return statusProperty;
	}

	public LocalDateTime getTimeOn() {
		return timeOn;
	}
	public String getTimeOnFormatted() {
		return formattedTime(getTimeOn());
	}

	public void setTimeOn(LocalDateTime timeOn) {
		this.timeOn = timeOn;
	}

	public String getValueOn() {
		return valueOn;
	}

	public void setValueOn(String valueOn) {
		this.valueOn = valueOn;
	}

	public LocalDateTime getTimeAck() {
		return timeAck;
	}
	public String getTimeAckFormatted() {
		return formattedTime(getTimeAck());
	}

	public void setTimeAck(LocalDateTime timeAck) {
		this.timeAck = timeAck;
	}

	public String getValueAck() {
		return valueAck;
	}

	public void setValueAck(String valueAck) {
		this.valueAck = valueAck;
	}

	public LocalDateTime getTimeOff() {
		return timeOff;
	}
	public String getTimeOffFormatted() {
		return formattedTime(getTimeOff());
	}

	public void setTimeOff(LocalDateTime timeOff) {
		this.timeOff = timeOff;
	}

	public String getValueOff() {
		return valueOff;
	}

	public void setValueOff(String valueOff) {
		this.valueOff = valueOff;
	}

	public void setStatusOff() {
		setStatus(STATUS_OFF);
	}

	public void setStatusOffNotAcked() {
		setStatus(STATUS_OFF_NOTACKED);
	}

	public void setStatusOnNotAcked() {
		setStatus(STATUS_ON_NOTACKED);
	}

	public void setStatusOnAcked() {
		setStatus(STATUS_ON_ACKED);
	}

	public String getSubscriptionName() {
		String name = "";
		if (subscriptionTag != null)
			if (subscriptionTag.getSubscription() != null)
				name = subscriptionTag.getSubscription().getSubscriptionName();
		return name;
	}

	public String getSubscriptionTagName() {
		String name = "";
		if (subscriptionTag != null)
			name = subscriptionTag.getTagName();
		return name;
	}

	public String getSubscriptionTagNameShort() {
		String name = getSubscriptionTagName();
		if (name != null && !name.isEmpty()) {
			String[] tagParts = name.split("\\.");
			name = tagParts[tagParts.length - 1];
		}
		return name;
	}
	
	public String getAlarmMessage() {
		String message = "";
		if (subscriptionTag != null)
			message = subscriptionTag.getAlarmMessage();
		return message;
	}

	public Observable[] getObservableProperties() {
		return observableProperties;
	}

	private String formattedTime(LocalDateTime time) {
		String s = "";
		if (time != null)
			s = time.format(timeFormatter);
		return s;
	}

}
