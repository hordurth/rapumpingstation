package com.roadauth.pumping.opcua.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prosysopc.ua.MonitoredItemBase;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.MonitoredDataItem;
import com.prosysopc.ua.client.MonitoredDataItemListener;
import com.prosysopc.ua.client.Subscription;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.UnsignedInteger;
import com.prosysopc.ua.stack.builtintypes.Variant;
import com.prosysopc.ua.stack.core.Attributes;
import com.prosysopc.ua.stack.core.MonitoringMode;
import com.roadauth.pumping.SpringContext;
import com.roadauth.pumping.opcua.data.model.OpcSubscriptionTag;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.WritableValue;

public class UASubscription {

	private Subscription subscription;
	private int namespaceId;
	private String subscriptionName;
	private HashMap<String, OpcSubscriptionTag> tagsByTagName;	// Key = Channel1.M340.MW2
	private HashMap<String, OpcSubscriptionTag> tagsByNodeId;	// Key = nodeId, ex: ns=2;s=Channel1.M340.MW2
	private HashMap<String, MonitoredDataItem> monitoredItemsByTagName;	// Key = Channel1.M340.MW2
	private HashMap<String, WritableValue<Object>> subscriptionTagPropertiesByTagName;		// Key = tag name, ex: Channel1.M340.MW2
	private HashMap<String, WritableValue<Object>> subscriptionTagPropertiesByNodeId;		// Key = nodeId, ex: ns=2;s=Channel1.M340.MW2
	private ArrayList<NodeId> nodes;

	private static Logger LOG = LoggerFactory.getLogger(UASubscription.class);

	public UASubscription() {
		subscriptionTagPropertiesByTagName = new HashMap<String, WritableValue<Object>>();
		subscriptionTagPropertiesByNodeId = new HashMap<String, WritableValue<Object>>();
	}

	public int getNamespaceId() { return namespaceId; }

	public void createSubscription(UaClient uaClient, int namespaceId, String subscriptionName, List<OpcSubscriptionTag> tagList) throws ServiceException, StatusException {
		MonitoredDataItem item;
		NodeId nodeId;
		nodes = new ArrayList<NodeId>();
		this.namespaceId = namespaceId;
		tagsByTagName = new HashMap<String, OpcSubscriptionTag>();
		tagsByNodeId = new HashMap<String, OpcSubscriptionTag>();
		monitoredItemsByTagName = new HashMap<String, MonitoredDataItem>();
		subscription = new Subscription();
		for (OpcSubscriptionTag tag : tagList) {
			nodeId = new NodeId(namespaceId, tag.getTagName());
			nodes.add(nodeId);
			item = new MonitoredDataItem(nodeId, Attributes.Value, MonitoringMode.Reporting);
			item.setDataChangeListener(dataChangeListener);
			subscription.addItem(item);
			tagsByTagName.put(tag.getTagName(), tag);
			tagsByNodeId.put(nodeId.toString(), tag);
			monitoredItemsByTagName.put(tag.getTagName(), item);
			LOG.info("Added subscription item: " + tag.getTagName());
		}
		uaClient.addSubscription(subscription);
	}

	public void checkStatus(UaClient uaClient) {
		if (subscription.isConnected()) {
			if (!subscription.isTimeout()) {
				LOG.trace("Subscription " + subscriptionName + ": OK");
			}
			else {
				LOG.error("Subscription " + subscriptionName + ": Timeout");
			}
		}
		else {
			LOG.error("Subscription " + subscriptionName + ": Not connected");
		}

	}

	public UASubscription restoreSubscription(UaClient uaClient) {
		// Remove the sub just in case, don't care about errors
		try { uaClient.removeSubscription(subscription); }
		catch (Exception ex) { /* Ignore exceptions here */ }

		// Remove all the items in the sub so that there aren't some rogue monitored data items
		// hooked up to the dataChangeListener and possibly reporting value changes
		for (int itemNo = 0; itemNo < subscription.getItemCount(); itemNo++) {
			try { subscription.removeItem(UnsignedInteger.valueOf(itemNo)); }
			catch (Exception ex) { /* Ignore exceptions here */ }
		}
		subscription = null;

		// Create the sub again
		UASubscription sub = new UASubscription();
		try {
			sub.createSubscription(uaClient, namespaceId, subscriptionName, new ArrayList<OpcSubscriptionTag>(tagsByTagName.values()));
		}
		catch (Exception ex)
		{ /* Ignore exceptions here */ }
		return sub;
	}

	public synchronized ArrayList<Object[]> getSubscriptionValues() {
		LOG.trace("VefgattUASubscription.getSubscriptionValues for sub: " + subscriptionName);
		ArrayList<Object[]> initialValues = new ArrayList<Object[]>();
		try {
			MonitoredItemBase[] items = subscription.getItems();
			if (items != null && items.length > 0) {
				//println("Adding " + items.length + " monitored item values to initialValues array");
				Object[] itemTagAndValue;
				MonitoredDataItem mdi;
				DataValue dv;
				Variant var;
				for (MonitoredItemBase item : items) {
					itemTagAndValue = new Object[2];
					try {
						mdi = (MonitoredDataItem)item;
						itemTagAndValue[0] = mdi.getNodeId().getValue();
						//println("mdi: " + mdi.toString());
						dv = mdi.getValue();
						if (dv != null) {
							//println("dv: " + dv.toString());
							var = dv.getValue();
							//println("var: " + var.toString());
							itemTagAndValue[1] = var.getValue();
							initialValues.add(itemTagAndValue);
							//println("Added initialValue: " + itemTagAndValue[1] + " for tag: " + itemTagAndValue[0]);
						}
						//else
							//println("No value available for tag: " + ((MonitoredDataItem)item).getNodeId().getValue());
					}
					catch (Exception ex) {
						LOG.error("Exception while getting value for tag: " + ((MonitoredDataItem)item).getNodeId().getValue(), ex);
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Exception in getSubscriptionValues", e);
		}

		LOG.trace("OpcServiceBean.getSubscriptionValues - Returning array with " + initialValues.size() + " items");

		return initialValues;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public synchronized WritableValue getSubscriptionTagProperty(String tagName, OpcUAService.TAGVALUETYPE type) {
		WritableValue prop = null;

		OpcSubscriptionTag tag = tagsByTagName.get(tagName);
		if (tag != null) {
			prop = subscriptionTagPropertiesByTagName.get(tagName);
			if (prop == null) {
				// Create the property based on the type
				switch (type) {
					case INTEGER:	prop = new SimpleIntegerProperty(); break;
					case LONG:		prop = new SimpleLongProperty(); break;
					case DOUBLE:	prop = new SimpleDoubleProperty(); break;
					case BOOLEAN:	prop = new SimpleBooleanProperty(); break;
					case STRING:	prop = new SimpleStringProperty(); break;
					case NONE:		prop = new SimpleDoubleProperty(); break;
				}

				// Get a value for the property if we have one
				MonitoredDataItem mdi = monitoredItemsByTagName.get(tagName);
				if (mdi != null) {
					DataValue dv = mdi.getValue();
					if (dv != null) {
						Variant var = dv.getValue();
						if (var != null)
							prop.setValue(var.getValue());
					}
				}

				subscriptionTagPropertiesByNodeId.put(mdi.getNodeId().toString(), prop);
				subscriptionTagPropertiesByTagName.put(tagName, prop);
			}
		}
		else
			LOG.error("Unknown tag: " + tagName);

		return prop;
	}

	private MonitoredDataItemListener dataChangeListener = new MonitoredDataItemListener() {
		@Override
		public void onDataChange(MonitoredDataItem sender, DataValue prevValue, DataValue value) {
			try {
				OpcUAService opcService = SpringContext.getBean(OpcUAService.class);

				String nodeId = sender.getNodeId().toString();

				LOG.trace("Monitored item changed: " + nodeId);
				LOG.trace("Prev value: " + prevValue);
				LOG.trace("New value: " + value);

				// See if someone is monitoring this tag and if so, update it
				WritableValue<Object> prop = subscriptionTagPropertiesByNodeId.get(nodeId);
				if (prop != null)
					Platform.runLater(new UATagUpdater(nodeId, prop, value.getValue().getValue()));

				// See if the tag is alarmEnabled and if so, check if an alarm exists or needs to be created or deactivated
				OpcSubscriptionTag tag = tagsByNodeId.get(nodeId);
				if (tag != null) {
					Integer alarmEnabled = tag.getAlarmEnabled();
					if (alarmEnabled != null && alarmEnabled.intValue() == 1)
						Platform.runLater(new AlarmChecker(opcService, tag, value));
				}
			}
			catch (Exception ex) {
				LOG.error("Error in dataChangeListener", ex);
			}
		}
	};

}
