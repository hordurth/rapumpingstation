package com.roadauth.pumping.opcua.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.Variant;
import com.roadauth.pumping.opcua.data.model.OpcAlarm;
import com.roadauth.pumping.opcua.data.model.OpcServer;
import com.roadauth.pumping.opcua.data.model.OpcServerHeartbeatTag;
import com.roadauth.pumping.opcua.data.model.OpcSubscription;
import com.roadauth.pumping.opcua.data.model.OpcSubscriptionTag;
import com.roadauth.pumping.opcua.data.model.OpcTagGroup;
import com.roadauth.pumping.opcua.data.repositories.OpcAlarmRepository;
import com.roadauth.pumping.opcua.data.repositories.OpcServerHeartbeatTagRepository;
import com.roadauth.pumping.opcua.data.repositories.OpcServerRepository;
import com.roadauth.pumping.opcua.data.repositories.OpcSubscriptionRepository;
import com.roadauth.pumping.opcua.data.repositories.OpcSubscriptionTagRepository;
import com.roadauth.pumping.opcua.data.repositories.OpcTagGroupRepository;

import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Callback;

@Service
public class OpcUAService {

	public static enum TAGVALUETYPE { NONE, INTEGER, LONG, DOUBLE, BOOLEAN, STRING };

	private static final int MAX_ALARM_VALUE_LENGTH = 50;
	
	public static final String OPC_SERVER = "kepware";
	public static final String OPC_SUBSCRIPTION = "Vegagerdin";

	@Autowired
	OpcServerRepository opcServerRepo;
	
	@Autowired
	OpcSubscriptionRepository opcSubscriptionRepo;
	
	@Autowired
	OpcSubscriptionTagRepository opcSubscriptionTagRepo;
	
	@Autowired
	OpcServerHeartbeatTagRepository opcServerHeartbeatTagRepo;
	
	@Autowired
	OpcTagGroupRepository opcTagGroupRepo;
	
	@Autowired
	OpcAlarmRepository opcAlarmRepo;

	private HashMap<String, String> serverUrlOverrides = new HashMap<String, String>();
	private HashMap<String, UAServer> serversByName;
	private HashMap<Integer, UAServer> serversById;
	private List<OpcServerHeartbeatTag> heartbeatTags;
	private HashMap<String, OpcTagGroup> tagGroups;
	private ObservableList<OpcAlarm> activeAlarms;
	private HashMap<String, OpcAlarm> activeAlarmsByTagName;

	private Timer heartbeatTimer = null;

	private static Logger logger = LoggerFactory.getLogger(OpcUAService.class);

	public OpcUAService() {
		logger.info("OpcService created");
	}

	public void addServerUrlOverride(String serverName, String serverUrl) {
		serverUrlOverrides.put(serverName, serverUrl);
	}

	public void connectToUAServers() {
		try {
			logger.info("Connecting to OpcUA servers");

			loadServers();
			loadSubscriptions();
			loadAlarms();
			loadServerHeartbeatTags();
			loadTagGroups();
		}
		catch (Exception ex) {
			logger.error("Exception occurred while connecting to OpcUA servers", ex);
		}
	}

	public void disconnectFromUAServers() {
		try {
			if (heartbeatTimer != null) {
				Timer hbt = heartbeatTimer;
				heartbeatTimer = null;
				hbt.cancel();
			}
		}
		catch (Exception ex) {
			logger.error("Exception while terminating heartbeat timer: " + ex.getMessage(), ex);
		}

		if (serversById != null) {
			for (UAServer server : serversById.values()) {
				try {
					server.disconnectFromUAServer();
				}
				catch (Exception ex) {
					logger.error("Failed to disconnect from UAServer: " + server.getServerName(), ex);
				}
			}
		}
	}

	public UAServer getServerById(Integer serverId) {
		return serversById.get(serverId);
	}

	public ArrayList<Object[]> getSubscriptionValues(String connectionName, String subscriptionName) {
		logger.info("OpcServiceBean.getSubscriptionValues connectionName: " + connectionName + ", subscriptionName: " + subscriptionName);
		UAServer server = serversByName.get(connectionName);
		if (server != null && server.serverIsOk()) {
			logger.info("OpcServiceBean.getSubscriptionValues - server found");
			UASubscription subscription = server.getSubscription(subscriptionName);
			if (subscription != null)
				return subscription.getSubscriptionValues();
		}

		return null;
	}

	public List<Object> readTagValues(String tagGroupName, List<String> tagNames) {
		List<Object> values = new ArrayList<Object>();

		for (String tagName : tagNames)
			values.add(readTagValue(tagGroupName, tagName));

		return values;
	}

	public Object readTagValue(String tagGroupName, String tagName) {
		Object value = null;
		UaClient uaClient = null;
		OpcTagGroup grp = tagGroups.get(tagGroupName);
		if (grp != null) {
			UAServer server = serversById.get(grp.getServerId());
			if (server != null)
				uaClient = server.getUaClient();

			if (uaClient != null && server.serverIsOk()) {
				String fullTagName = grp.getFullTagName(tagName);
				try {
					logger.info("Reading value of tag: " + tagName + " in group: " + grp.getGroupName());
					NodeId nodeId = new NodeId(grp.getNameSpaceId(), fullTagName);
					DataValue dv = uaClient.readValue(nodeId);
					value = dv.getValue().getValue();

					logger.info("Returning tag value: " + value.toString());
				}
				catch (Exception ex) {
					logger.error("Unable to get value for tag: " + tagName + " in group: " + grp.getGroupName(), ex);
				}
			}
			else
				logger.info("readTagValue: Not connected to server");
		}

		return value;
	}

	public boolean writeTagGroupTagValue(String tagGroupName, String tagName, Object tagValue) {
		boolean result = false;
		UaClient uaClient = null;
		OpcTagGroup grp = tagGroups.get(tagGroupName);
		if (grp != null) {
			UAServer server = serversById.get(grp.getServerId());
			if (server != null)
				uaClient = server.getUaClient();

			if (uaClient != null && server.serverIsOk()) {
				String fullTagName = grp.getFullTagName(tagName);
				try {
					NodeId nodeId = new NodeId(grp.getNameSpaceId(), fullTagName);
					Variant varTagValue = new Variant(tagValue);
					uaClient.writeValue(nodeId, varTagValue);
					result = true;
				}
				catch (Exception ex){
					logger.error("Unable to set value for tag: " + tagName + " in group: " + grp.getGroupName() + " to value: " + tagValue + " of type: " + tagValue.getClass().getName(), ex);
				}
			}
		}

		return result;
	}

	public boolean writeSubscriptionTagValue(String serverName, String subscriptionName, String tagName, Object tagValue) {
		boolean result = false;
		UAServer server = getServerByName(serverName);
		if (server != null) {
			UASubscription sub = server.getSubscription(subscriptionName);
			if (sub != null) {
				UaClient uaClient = server.getUaClient();
				if (uaClient != null && server.serverIsOk()) {
					try {
						NodeId nodeId = new NodeId(sub.getNamespaceId(), tagName);
						Variant varTagValue = new Variant(tagValue);
						uaClient.writeValue(nodeId, varTagValue);
						result = true;
					}
					catch (Exception ex){
						logger.error("Unable to set value for tag: " + tagName + " in subscription: " + subscriptionName + " to value: " + tagValue + " of type: " + tagValue.getClass().getName(), ex);
					}
				}
			}
		}

		return result;
	}

	@SuppressWarnings("rawtypes")
	public ObservableValue getSubscriptionTagProperty(String serverName, String subscriptionName, String tagName, OpcUAService.TAGVALUETYPE type) {
		ObservableValue prop = null;

		UAServer server = getServerByName(serverName);
		if (server != null) {
			UASubscription sub = server.getSubscription(subscriptionName);
			if (sub != null) {
				// TODO Find better way of doing this, I know that getSubTagProp returns an ObservableValue (SimpleIntegerProperty and such) but this is still not good design
				prop = (ObservableValue)sub.getSubscriptionTagProperty(tagName, type);
			}
			else
				System.err.println("Unknown subscription: " + subscriptionName);
		}
		else
			System.err.println("Unknown server: " + serverName);

		return prop;
	}

	public UAServer getServerByName(String serverName) {
		UAServer server = null;
		if (serverName != null && serverName.length() > 0)
			server = serversByName.get(serverName);
		else
			server = (UAServer)serversByName.values().toArray()[0];		// Get the first known server, might be the only one
		return server;
	}

	private void loadServers() throws Exception {
		serversByName = new HashMap<String, UAServer>();
		serversById = new HashMap<Integer, UAServer>();

		// Get the OPCUA server URIs from the database
		List<OpcServer> servers = opcServerRepo.findAllEnabled();
		logger.info("Found " + (servers != null ? servers.size() : 0) + " servers");

		// Create UAServer objects
		if (servers != null) {
			UAServer uaServer;
			String overrideUrl;
			for (OpcServer server : servers) {
				// Change the server url if overridden
				overrideUrl = serverUrlOverrides.get(server.getServerName());
				if (overrideUrl != null && !overrideUrl.isEmpty())
					server.setServerUrl(overrideUrl);

				// Create the server/OPCUA client
				uaServer = new UAServer(server);
				uaServer.connectToUAServer();
				serversByName.put(uaServer.getServerName(), uaServer);
				serversById.put(uaServer.getServerId(), uaServer);
			}
		}
	}

	private void loadSubscriptions() throws ServiceException, StatusException {
		// Get subscriptions from the database
		List<OpcSubscription> subscriptions = opcSubscriptionRepo.findAll();
		logger.info("Found " + (subscriptions != null ? subscriptions.size() : 0) + " subscriptions");

		// Create the UASubscription objects
		if (subscriptions != null) {
			UAServer uaServer;
			for (OpcSubscription subscription : subscriptions) {
				// Create the subscription
				uaServer = serversById.get(subscription.getServerId());
				if (uaServer != null) {
					// Get the subscription tags
					List<OpcSubscriptionTag> tags = opcSubscriptionTagRepo.findAllForSubscription(subscription);
					logger.info("Found subscription " + subscription.getSubscriptionName() + " @ " + uaServer.getServerName() + " with " + tags.size() + " tags");
					if (tags.size() > 0)
						uaServer.addSubscription(subscription, tags);
				}
				else
					logger.info("Server not found for subscriptionname=" + subscription.getSubscriptionName());
			}
		}
	}

	private void loadAlarms() {
		// Create an extractor that returns all observable properties of an alarm.
		// This is so both the alarm items and some of their properties are observable, not just the alarm items.
		Callback<OpcAlarm, Observable[]> extractor = new Callback<OpcAlarm, Observable[]>() {
			@Override
			public Observable[] call(OpcAlarm alarm) {
				return alarm.getObservableProperties();
			}
		};

		activeAlarms = FXCollections.observableArrayList(extractor);
		activeAlarmsByTagName = new HashMap<String, OpcAlarm>();

		List<OpcAlarm> alarms = opcAlarmRepo.findAllActive();
		for (OpcAlarm alarm : alarms) {
			activeAlarms.add(alarm);
			activeAlarmsByTagName.put(alarm.getSubscriptionTagName(), alarm);
		}
	}

	private void loadServerHeartbeatTags() {
		heartbeatTags = opcServerHeartbeatTagRepo.findAllEnabled();
		if (heartbeatTags.size() > 0) {
			TimerTask task = new TimerTask() {
				@Override public void run() {
					checkServerHeartbeatTags();
				}
			};
			if (heartbeatTimer == null) {
				heartbeatTimer = new Timer();
				heartbeatTimer.schedule(task, 5 * 1000, 5 * 1000);
			}
		}
	}

	private void loadTagGroups() {
		tagGroups = new HashMap<String, OpcTagGroup>();

		List<OpcTagGroup> rows = opcTagGroupRepo.findAll();
		for (OpcTagGroup row : rows)
			tagGroups.put(row.getGroupName(), row);
	}

	// TODO - need other way of doing this w/o EJB container: @Schedule(second = "0,5,10,15,20,25,30,35,40,45,50,55", minute = "*", hour = "*", persistent = false)
	@SuppressWarnings("unused")
	private void checkServerHeartbeatTags() {
		if (heartbeatTags == null || heartbeatTags.size() == 0)
			return;

		for (OpcServerHeartbeatTag row : heartbeatTags)
			checkServerHeartbeatTag(row);
	}

	private void checkServerHeartbeatTag(OpcServerHeartbeatTag sht) {
		boolean serverIsOk = false;
		UAServer vgServer = serversById.get(sht.getServerId());
		UaClient uaClient = vgServer.getUaClient();
//		logger.info("Checking tag: " + sht.getTag() + " in server: " + vgServer.getServerName());
		try {
//			logger.info("Getting sht value");
			DataValue value = uaClient.readValue(new NodeId(sht.getNamespaceId(), sht.getTag()));
//			if (value != null) logger.info("Sht value: " + value);
//			else logger.info("Sht value is NULL");
			if (!(value == null || value.isNull())) {
//				logger.info("Getting last value");
				DataValue lastValue = sht.getLastValue();
//				if (lastValue != null) logger.info("Last value: " + lastValue);
//				else logger.info("Last value is NULL");
				if (lastValue != null) {
//					logger.info("Checking value against last value");
//					if (value.getValue().equals(lastValue.getValue()))
//						logger.info("Value == lastValue: " + value.toString());
//					else {
						serverIsOk = true;
//						logger.info("Value is ok: " + value.toString());
//					}
//					logger.info("Done checking value against last value");
				}
				else {
					serverIsOk = true;
//					logger.info("Storing value for first time");
				}
//				logger.info("Done checking sht value");
			}
			else
//				logger.info("Read tag value is null");
//			logger.info("Setting sht last value to: " + value);
			sht.setLastValue(value);
//			logger.info("Done setting sht last value to: " + value);
		} catch (ServiceException ex) {
			logger.info("ServiceException in CheckServerHeartbeatTag");
			logger.error("", ex);

			// Try reconnecting, this is probably a session timeout
			logger.info("Session timeout, trying to reconnect...");
			try {
				vgServer.reconnectToUAServer();
				serverIsOk = true;
			}
			catch (Exception ex2) {
				logger.error("Exception in CheckServerHeartbeatTag when trying to reconnect to server", ex2);
			}
		}
		catch (Exception ex3) {
			logger.error("Exception in CheckServerHeartbeatTag", ex3);
		}

		// Set the serverIsUp status according to if we were able to successfully read a value from it
		// and that value was different from the last read value.
//		logger.info("Setting server is up flag to: " + serverIsOk);
		vgServer.setServerIsUp(serverIsOk);
//		logger.info("Done setting server is up flag to: " + serverIsOk);

		if (!serverIsOk)
			logger.info("Server is OFFLINE!!!");
//		logger.info("End of CheckServerHeartbeatTag");
	}

	public ObservableList<OpcAlarm> getActiveAlarmsList() {
		return activeAlarms;
	}

	// TODO Need to think about thread-safety of the following alarm functions
	public OpcAlarm getAlarmForTagName(String tagName) {
		return activeAlarmsByTagName.get(tagName);
	}

	public void createAlarm(OpcSubscriptionTag tag, DataValue value) {
		LocalDateTime timeOn = OpcUAUtils.opcDateTimeToLocalDateTime(value.getSourceTimestamp());
		OpcAlarm alarm = new OpcAlarm();
		alarm.setSubscriptionTag(tag);
		alarm.setTimeOn(timeOn);
		alarm.setValueOn(OpcUAUtils.getTagValueAsString(value));
		opcAlarmRepo.save(alarm);

		activeAlarms.add(0, alarm);
		activeAlarmsByTagName.put(tag.getTagName(), alarm);
	}

	public void setAlarmStatusOn(OpcAlarm alarm, DataValue value) {
		if (alarm.getTimeAck() != null)
			alarm.setStatusOnAcked();
		else
			alarm.setStatusOnNotAcked();
		alarm.setTimeOn(OpcUAUtils.opcDateTimeToLocalDateTime(value.getSourceTimestamp()));
		String s = value.getValue().getValue().toString();
		if (s != null && s.length() > MAX_ALARM_VALUE_LENGTH)
			s = s.substring(0, MAX_ALARM_VALUE_LENGTH);
		alarm.setValueOn(s);
		opcAlarmRepo.save(alarm);
	}

	public void acknowledgeAlarm(OpcAlarm alarm) {
		alarm.setTimeAck(LocalDateTime.now());

		// Set the value of the tag at the time of the Ack, get the tag from the subscription and get it's current value
		String ackValue = null;
		UAServer server = serversById.get(alarm.getId());
		if (server != null) {
			UASubscription sub = server.getSubscriptionById(alarm.getSubscriptionTag().getSubscription().getId());
			if (sub != null) {
				// TODO Tag type should not be fixed here, boolean may be a fairly safe bet for alarms but not 100% safe
				ObservableValue val = opcTagValue(alarm.getSubscriptionTagName(), TAGVALUETYPE.BOOLEAN);
				if (val != null) {
					ackValue = val.getValue().toString();
					if (ackValue != null && ackValue.length() > MAX_ALARM_VALUE_LENGTH)
						ackValue = ackValue.substring(0, MAX_ALARM_VALUE_LENGTH);
				}
			}
		}
		alarm.setValueAck(ackValue);

		if (alarm.getTimeOff() != null) {
			// Alarm is off and has been acked, remove from the active alarm lists
			alarm.setStatusOff();
			removeAlarm(alarm);
		}
		else {
			// Alarm has not gone off but is now acked
			alarm.setStatusOnAcked();
			opcAlarmRepo.save(alarm);
		}
	}

	public void setAlarmStatusOff(OpcAlarm alarm, DataValue value) {
		if (alarm.getTimeAck() != null)
			alarm.setStatusOff();
		else
			alarm.setStatusOffNotAcked();
		alarm.setTimeOff(OpcUAUtils.opcDateTimeToLocalDateTime(value.getSourceTimestamp()));
		String s = value.getValue().getValue().toString();
		if (s != null && s.length() > MAX_ALARM_VALUE_LENGTH)
			s = s.substring(0, MAX_ALARM_VALUE_LENGTH);
		alarm.setValueOff(s);

		if (alarm.getTimeAck() != null) {
			// Status is off and has been acked, remove from the active alarm lists
			removeAlarm(alarm);
		}
		else
			opcAlarmRepo.save(alarm);
	}

	synchronized private void removeAlarm(OpcAlarm alarm) {
		activeAlarms.remove(alarm);
		activeAlarmsByTagName.remove(alarm.getSubscriptionTagName());
		
		opcAlarmRepo.save(alarm);
	}

	public boolean isServerOk(String connectionName) {
		boolean result = false;
		UAServer server = serversByName.get(connectionName);

		if (server != null) {
			result = server.serverIsOk();
			logger.info("isServerOk: result=" + result + " for connectionName=" + connectionName);
		}
		else
			logger.info("isServerOk: Server not found for connectionName=" + connectionName);

		return result;
	}

	@SuppressWarnings("rawtypes")
	public synchronized ObservableValue opcTagValue(String opcTagName, OpcUAService.TAGVALUETYPE opcTagType) {
		ObservableValue opcTagValue = null;

		try {
			if (opcTagName != null && opcTagName.length() > 0) {
				logger.info("Getting value for opc tag: " + opcTagName + " of type: " + opcTagType);
				opcTagValue = getSubscriptionTagProperty(OPC_SERVER, OPC_SUBSCRIPTION, opcTagName, opcTagType);
			}
		}
		catch (Exception ex) {
			// TODO Might be running inside Scene Builder and then we will get an exception on the above OpcService stuff, must handle or Scene Builder goes grumpy
		}
		
		return opcTagValue;
	}
	
	public synchronized void setTagValue(String opcTagName, Object tagValue) {
		logger.info("Setting subscription tag value, tag: " + opcTagName + ", value: " + tagValue.toString());
		writeSubscriptionTagValue(OPC_SERVER, OPC_SUBSCRIPTION, opcTagName, tagValue);
	}

}
