package com.roadauth.pumping.opcua.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.value.WritableValue;

public class UATagUpdater implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(UATagUpdater.class);

	private String tag;
	private WritableValue<Object> property;
	private Object value;

	public UATagUpdater(String tag, WritableValue<Object> property, Object value) {
		this.tag = tag;
		this.property = property;
		this.value = value;
	}

	@Override
	public void run() {
		try {
			logger.info("Setting tag: " + tag + " with property: " + property + " to value: " + (value != null ? value : "NULL"));
			property.setValue(value);
		}
		catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
	}

}
