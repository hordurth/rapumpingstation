package com.roadauth.pumping.opcua.service;

import java.time.LocalDateTime;
import java.util.GregorianCalendar;

import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.DateTime;
import com.prosysopc.ua.stack.builtintypes.Variant;

public class OpcUAUtils {

	public static LocalDateTime opcDateTimeToLocalDateTime(DateTime opcDateTime) {
		LocalDateTime ldt = null;

		if (opcDateTime != null) {
			GregorianCalendar cal = opcDateTime.getLocalCalendar();
			if (cal != null)
				ldt = cal.toZonedDateTime().toLocalDateTime();
		}

		return ldt;
	}
	
	public static String getTagValueAsString(DataValue tagValue) {
		String strValue = null;
		
		if (tagValue != null) {
			Variant varValue = tagValue.getValue();
			if (varValue != null) {
				Object objValue = varValue.getValue();
				if (objValue != null)
					strValue = objValue.toString();
			}
		}
		
		return strValue;
	}

}
