package com.roadauth.pumping.opcua.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.Variant;
import com.roadauth.pumping.opcua.data.model.OpcAlarm;
import com.roadauth.pumping.opcua.data.model.OpcSubscriptionTag;

/**
 * OPC AlarmChecker
 * 
 * @author hordur
 *
 *         This version of the AlarmChecker allows for two types of alarms, an
 *         analog alarm with an integer value that is considered ON if the value
 *         of the tag equals 1, and a discrete alarm with a boolean value that
 *         is considered ON if the value of the tag equals TRUE. We assume that
 *         the monitoring of actual PLC values like tank volumes and such will
 *         actually happen in the PLC and it will then turn on an alarm tag if
 *         it finds that another tag has gone under/over it's allowed range.
 * 
 *         This guy gets created when a UASubscription instance gets a
 *         notification that a subscribed tag has a new value and that tag is
 *         alarmEnabled.
 */

public class AlarmChecker implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(AlarmChecker.class);

	private static final Variant INT_ONE = new Variant(1);
	private static final Variant BOOL_TRUE = new Variant(true);

	private OpcUAService opcService;
	private OpcSubscriptionTag tag;
	private DataValue value;

	public AlarmChecker(OpcUAService opcService, OpcSubscriptionTag tag, DataValue value) {
		this.opcService = opcService;
		this.tag = tag;
		this.value = value;
	}

	@Override
	public void run() {
		Variant varValue = value.getValue();
		if (varValue != null) {
			OpcAlarm alarm = opcService.getAlarmForTagName(tag.getTagName());

			if (varValue.compareTo(INT_ONE) == 0 || varValue.compareTo(BOOL_TRUE) == 0) {
				// Alarm just came on. See if we have an active alarm for the tag and if not,
				// create it. Note that we could
				// have an active alarm for the tag that went off previously but hasn't been
				// acked by the user.
				if (alarm == null) {
					// Create a new alarm
					logger.info("Creating alarm: " + tag.getTagName());
					opcService.createAlarm(tag, value);
				}
				else {
					// Have alarm, just change it's state and time info
					logger.info("Turning alarm on: " + alarm.getSubscriptionTagName());
					opcService.setAlarmStatusOn(alarm, value);
				}
			}
			else {
				// Alarm just went off
				if (alarm != null) {
					logger.info("Turning alarm off: " + alarm.getSubscriptionTagName());
					opcService.setAlarmStatusOff(alarm, value);
				}
			}
		}
	}

}
