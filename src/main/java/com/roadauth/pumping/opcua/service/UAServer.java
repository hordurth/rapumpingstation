package com.roadauth.pumping.opcua.service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.client.ServerStatusListener;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.LocalizedText;
import com.prosysopc.ua.stack.builtintypes.StatusCode;
import com.prosysopc.ua.stack.cert.DefaultCertificateValidator;
import com.prosysopc.ua.stack.cert.PkiDirectoryCertificateStore;
import com.prosysopc.ua.stack.core.ApplicationDescription;
import com.prosysopc.ua.stack.core.ApplicationType;
import com.prosysopc.ua.stack.core.ServerState;
import com.prosysopc.ua.stack.core.ServerStatusDataType;
import com.prosysopc.ua.stack.transport.security.SecurityMode;
import com.roadauth.pumping.opcua.data.model.OpcServer;
import com.roadauth.pumping.opcua.data.model.OpcSubscription;
import com.roadauth.pumping.opcua.data.model.OpcSubscriptionTag;

import javafx.beans.property.SimpleBooleanProperty;

public class UAServer {

	private static String APP_NAME = "Vegagerðin Dælustöðvar";
	private static SecurityMode SECURITY_MODE = SecurityMode.NONE;

	private Integer serverId;
	private String serverName;
	private String serverUrl;
	private int sessionCount = 0;

	private UaClient uaClient;
	private HashMap<String, UASubscription> subscriptions;
	private HashMap<Integer, UASubscription> subscriptionsById;
	private SimpleBooleanProperty serverIsUp;
	private SimpleBooleanProperty tryingToReconnect;

	private static Logger LOG = LoggerFactory.getLogger(UAServer.class);

	public UAServer(OpcServer server) {
		subscriptions = new HashMap<String, UASubscription>();
		subscriptionsById = new HashMap<Integer, UASubscription>();

		serverId = server.getId();
		serverName = server.getServerName();
		serverUrl = server.getServerUrl();
	}

	public UaClient getUaClient() { return uaClient; }
	public Integer getServerId() { return serverId; }
	public String getServerName() { return serverName; }
	public String getServerUrl() { return serverUrl; }
	public SimpleBooleanProperty getServerIsUpProperty() { return serverIsUp; }
	public SimpleBooleanProperty getTryingToReconnectProperty() { return tryingToReconnect; }
	public UASubscription getSubscription(String subscriptionName) {
		if (subscriptionName != null && subscriptionName.length() > 0)
			return subscriptions.get(subscriptionName);
		else
			return (UASubscription)subscriptions.values().toArray()[0];		// Return first sub, might be the only one
	}
	public UASubscription getSubscriptionById(Integer subscriptionId) {
		if (subscriptionId != null && subscriptionId.intValue() > 0)
			return subscriptionsById.get(subscriptionId);
		else
			return (UASubscription)subscriptionsById.values().toArray()[0];		// Return first sub, might be the only one
	}

	public void connectToUAServer() throws Exception {
		this.serverIsUp = new SimpleBooleanProperty(false);
		this.tryingToReconnect = new SimpleBooleanProperty(false);

		LOG.info("Connecting to OPC-UA server " + serverName + " @ " + serverUrl);

		// Create the UaClient
		uaClient = new UaClient(serverUrl);

		// Use PKI files to keep track of the trusted and rejected server certificates. The files will be managed
		// in a directory structure under the base directory specified as the constructor parameter.
		//final PkiFileBasedCertificateValidator validator = new PkiFileBasedCertificateValidator("efla");
		//uaClient.setCertificateValidator(validator);

		final PkiDirectoryCertificateStore certStore = new PkiDirectoryCertificateStore();
		final DefaultCertificateValidator validator = new DefaultCertificateValidator(certStore);


		// React to validation results with a custom handler (deal with validation events if necessary)
		//validator.setValidationListener(validationListener);
		try {
			uaClient.setCertificateValidator(validator);
		}
		catch (Throwable ex) {
			LOG.error("Failed to connect to OPC-UA server", ex);
			throw new Exception("Unable to connect to OPC-UA server");
		}

		// The ApplicationDescription is sent to the server...
		ApplicationDescription appDescription = new ApplicationDescription();
		appDescription.setApplicationName(new LocalizedText(APP_NAME, Locale.ENGLISH));
		// 'localhost' (all lower case) in the URI is converted to the actual
		// host name of the computer in which the application is run
		appDescription.setApplicationUri("urn:localhost:UA:" + APP_NAME);
		appDescription.setProductUri("urn:efla.is:UA:" + APP_NAME);
		appDescription.setApplicationType(ApplicationType.Client);

		// Define the client application identity, including the security certificate
		final ApplicationIdentity identity = ApplicationIdentity.loadOrCreateCertificate(appDescription, "Efla",
				/* Private Key Password */null,
				/* Key File Path */new File(certStore.getBaseDir(), "private"),
				/* Enable renewing the certificate */true);

		uaClient.setApplicationIdentity(identity);

		// Define our user locale - the default is Locale.getDefault()
		uaClient.setLocale(Locale.ENGLISH);

		// Define a default communication timeout in milliseconds
		uaClient.setTimeout(10000);

		// Default session timeout is 1 hour, set to 24 hours so that we don't loose the connection for 2 seconds every hour
		uaClient.setSessionTimeout(5 * 1000 * 60 /*24 * 60 * 60 * 1000*/);
		LOG.info("UaClient session timeout set, is now: " + uaClient.getSessionTimeout() / 1000 + " seconds");

		// Listen to server status changes
		uaClient.addServerStatusListener(serverStatusListener);

		// Define the security mode
		// - Default (in UaClient) is BASIC128RSA15_SIGN_ENCRYPT
		// client.setSecurityMode(SecurityMode.BASIC128RSA15_SIGN_ENCRYPT);
		// client.setSecurityMode(SecurityMode.BASIC128RSA15_SIGN);
		// client.setSecurityMode(SecurityMode.NONE);
		uaClient.setSecurityMode(SECURITY_MODE);

		// If the server supports user authentication, you can set the user
		// identity.
		// - Default is to use Anonymous authentication, like this:
		uaClient.setUserIdentity(new UserIdentity());
		// - Use username/password authentication (note requires security,
		// above):
		// client.setUserIdentity(new UserIdentity("my_name", "my_password"));
		// - Read the user certificate and private key from files:
		// client.setUserIdentity(new UserIdentity(new
		// java.net.URL("my_certificate.der"),
		// new java.net.URL("my_privatekey.pfx"), "my_privatekey_password"));

		// We can define the session name that is visible in the server
		// as
		// well
		uaClient.setSessionName(APP_NAME + " Session" + ++sessionCount);
		uaClient.connect();

		serverIsUp.set(true);

		LOG.info(String.format("Server %s: Successfully connected to OPC-UA server " + serverName, serverName));

		// TODO Create event listener to get Alarm notifications.  Was going to use this with KepserverEx but
		// the OPC UA wrapper there doesn't support the UA Alarms and Conditions spec :-(
		// createEventListener();
	}

	public void disconnectFromUAServer() {
		getUaClient().disconnect();
	}

	public void reconnectToUAServer() {
		this.tryingToReconnect.set(true);
		try {
			LOG.info("Trying to reconnect to OPC UA server to re-initialize the session timeout");
			if (!getUaClient().reconnect())
				throw new Exception("Failed to reconnect to OPC UA server");
			else
				LOG.info("Re-connected successfully to OPC UA server");
		}
		catch (Exception ex) {
			LOG.error(this.getClass().getName(), "reconnectToUAServer", ex);
		}
		this.tryingToReconnect.set(false);
	}

	/*private com.prosysopc.ua.client.Subscription eventSub;
	public void createEventListener() throws Exception {
		MonitoredEventItem eventItem = new MonitoredEventItem(Identifiers.BaseEventType);
		eventItem.addEventListener(eventListener);
		eventSub = new com.prosysopc.ua.client.Subscription();
		eventSub.addItem(eventItem);
		uaClient.addSubscription(eventSub);
	}
	private static MonitoredEventItemListener eventListener = new MonitoredEventItemListener() {

		@Override
		public void onEvent(MonitoredEventItem eventSender, Variant[] eventFields) {
			LOG.info("Event sender: " + eventSender.toString());
			LOG.info("Event fields: " + eventFields.toString());
		}
	};*/

	public void addSubscription(OpcSubscription subscription, List<OpcSubscriptionTag> tags) throws ServiceException, StatusException {
		if (uaClient != null && serverIsUp.get()) {
			UASubscription sub = new UASubscription();
			sub.createSubscription(uaClient, subscription.getNamespaceId(), subscription.getSubscriptionName(), tags);
			subscriptions.put(subscription.getSubscriptionName(), sub);
			subscriptionsById.put(subscription.getId(), sub);
			LOG.info(String.format("Server %s: Added subscription " + subscription.getSubscriptionName(), serverName));
		}
	}

	/*private void checkSubscriptions() {
		VefgattUASubscription sub;
		int subNo;
		for (subNo = 0; subNo < subscriptions.size(); subNo++) {
			sub = subscriptions.get(subNo);
			sub.checkStatus(uaClient);
			subscriptions.set(subNo, sub.restoreSubscription(uaClient));
		}
	}*/

	/**
	 * A sample listener for server status changes.
	 */
	private ServerStatusListener serverStatusListener = new ServerStatusListener() {

		@Override
		public void onShutdown(UaClient uaClient, long secondsTillShutdown, LocalizedText shutdownReason) {
			// Called when the server state changes to Shutdown
			LOG.info(String.format("Server %s: Server shutdown in %d seconds. Reason: %s\n", serverName, secondsTillShutdown, shutdownReason.getText()));
			serverIsUp.set(false);
		}

		@Override
		public void onStateChange(UaClient uaClient, ServerState oldState, ServerState newState) {
			// Called whenever the server state changes
			LOG.info(String.format("Server %s: ServerState changed from %s to %s\n", serverName, oldState, newState));
			if (!newState.equals(ServerState.Running)) {
				LOG.error("ServerStatusError: " + uaClient.getServerStatusError());
				try {
					LOG.info("Possibly just session timeout, trying to reconnect...");
					reconnectToUAServer();
					LOG.info("Successfully reconnected");
				}
				catch (Exception ex) {
					LOG.info("Unable to reconnect, setting serverIsUp to false");
					serverIsUp.set(false);
				}
			}
			else {
				serverIsUp.set(true);
				//checkSubscriptions();
			}
		}

		@Override
		public void onStatusChange(UaClient client, ServerStatusDataType status, StatusCode errorCode) {
			// Called whenever the server status changes, typically every StatusCheckInterval defined in the UaClient.
			LOG.trace("ServerStatus: " + status);
		}
	};

	/*private CertificateValidationListener validationListener = new CertificateValidationListener() {

		@Override
		public ValidationResult onValidate(Cert certificate,
				ApplicationDescription applicationDescription,
				EnumSet<CertificateCheck> passedChecks) {
			// Called whenever the PkiFileBasedCertificateValidator has
			// validated a certificate
			LOG.fine("");
			LOG.fine("*** The Server Certificate : ");
			LOG.fine("");
			LOG.fine("Subject   : "
					+ certificate.getCertificate().getSubjectX500Principal()
							.toString());
			LOG.fine("Issued by : "
					+ certificate.getCertificate().getIssuerX500Principal()
							.toString());
			LOG.fine("Valid from: "
					+ certificate.getCertificate().getNotBefore().toString());
			LOG.fine("        to: "
					+ certificate.getCertificate().getNotAfter().toString());
			LOG.fine("");
			if (!passedChecks.contains(CertificateCheck.Signature))
				LOG.severe("* The Certificate is NOT SIGNED BY A TRUSTED SIGNER!");
			if (!passedChecks.contains(CertificateCheck.Validity)) {
				Date today = new Date();
				final boolean isYoung = certificate.getCertificate()
						.getNotBefore().compareTo(today) > 0;
				final boolean isOld = certificate.getCertificate()
						.getNotAfter().compareTo(today) < 0;
				final String oldOrYoung = isOld ? "(anymore)"
						: (isYoung ? "(yet)" : "");

				LOG.severe("* The Certificate time interval IS NOT VALID "
						+ oldOrYoung + "!");
			}
			if (!passedChecks.contains(CertificateCheck.Uri)) {
				LOG.severe("* The Certificate URI DOES NOT MATCH the server URI!");
				LOG.severe("  serverURI="
						+ applicationDescription.getApplicationUri());
			}
			if (!passedChecks.contains(CertificateCheck.SelfSigned))
				LOG.severe("* The Certificate is self-signed.");
			LOG.fine("");
			// If the certificate is trusted, valid and verified, accept it
			if (passedChecks.containsAll(CertificateCheck.COMPULSORY))
				return ValidationResult.AcceptPermanently;
			do {
				LOG.fine("Note: If the certificate is not OK,");
				LOG.fine("you will be prompted again, even if you answer 'Always' here.");
				LOG.fine("");
				LOG.fine("Do you want to accept this certificate?\n"
						+ " (A=Always, Y=Yes, this time, N=No)\n"
						+ " (D=Show Details of the Certificate)");
				LOG.fine("AUTO RESPONSE = AcceptPermanently");
				return ValidationResult.AcceptPermanently;
				/*String input = readInput().toLowerCase();
				if (input.equals("a"))
					// if the certificate is not valid anymore or the signature
					// is not verified, you will be prompted again, even if you
					// select always here
					return ValidationResult.AcceptPermanently;

				if (input.equals("y"))
					return ValidationResult.AcceptOnce;
				if (input.equals("n"))
					return ValidationResult.Reject;
				if (input.equals("d"))
					logger.debug("Certificate Details:"
							+ certificate.getCertificate().toString());*/
	/*		} while (true);
		}

	};*/

	public boolean serverIsOk() {
		return serverIsUp.get() && !tryingToReconnect.get();
	}

	public void setServerIsUp(boolean isUp) {
		if (this.serverIsUp.get() != isUp)
			LOG.info("Changing serverIsUp from: " + this.serverIsUp.get() + " to: " + isUp);
		this.serverIsUp.set(isUp);
	}

}
