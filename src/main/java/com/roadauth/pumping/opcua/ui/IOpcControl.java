package com.roadauth.pumping.opcua.ui;

import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;

public interface IOpcControl {

	public String getOpcServer();

	public void setOpcServer(String t);

	public String getOpcSubscription();

	public void setOpcSubscription(String t);

	public String getOpcTagName();

	public void setOpcTagName(String t);

	public TAGVALUETYPE getOpcTagType();

	public void setOpcTagType(TAGVALUETYPE t);

	public void opcInit();

}
