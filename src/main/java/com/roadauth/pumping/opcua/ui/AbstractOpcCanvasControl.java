package com.roadauth.pumping.opcua.ui;

import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;

import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;

@SuppressWarnings("rawtypes")
abstract public class AbstractOpcCanvasControl extends Canvas implements IOpcControl {

	protected String opcServer = "";
	protected String opcSubscription = "";
	protected String opcTagName = "";
	protected TAGVALUETYPE opcTagType = TAGVALUETYPE.NONE;

	protected ObservableValue opcTagValue = null;

	public AbstractOpcCanvasControl() {
		super();
	}

	public AbstractOpcCanvasControl(double width, double height) {
		super(width, height);
	}

	@Override
	public String getOpcServer() {
		return opcServer;
	}

	@Override
	public void setOpcServer(String t) {
		opcServer = t;
	}

	@Override
	public String getOpcSubscription() {
		return opcSubscription;
	}

	@Override
	public void setOpcSubscription(String t) {
		opcSubscription = t;
	}

	@Override
	public String getOpcTagName() {
		return opcTagName;
	}

	@Override
	public void setOpcTagName(String t) {
		opcTagName = t;
	}

	@Override
	public TAGVALUETYPE getOpcTagType() {
		return opcTagType;
	}

	@Override
	public void setOpcTagType(TAGVALUETYPE t) {
		opcTagType = t;
		opcInit();
	}

}
