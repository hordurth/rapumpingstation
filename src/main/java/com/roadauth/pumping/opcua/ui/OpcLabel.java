package com.roadauth.pumping.opcua.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.roadauth.pumping.SpringContext;
import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

@SuppressWarnings("rawtypes")
public class OpcLabel extends Label implements IOpcControl {

	private static final Logger logger = LoggerFactory.getLogger(OpcLabel.class);

	protected String opcNumberFormat = "";
	protected boolean opcEditable = false;
	protected String opcServer = "";
	protected String opcSubscription = "";
	protected String opcTagName = "";
	protected TAGVALUETYPE opcTagType = TAGVALUETYPE.NONE;

	protected ObservableValue opcTagValue = null;

	protected StringProperty currentValue = new SimpleStringProperty();

	// TODO Currently relying on opcTagType being set last to call opcInit, find
	// better way!!
	public void opcInit() {
		try {
			// Add monitor for currentValue
			currentValue.addListener(currentValueListener);

			// Bind currentValue to the opcTagValue
			OpcUAService opcService = SpringContext.getBean(OpcUAService.class);
			opcTagValue = opcService.opcTagValue(opcTagName, opcTagType);
			if (opcTagValue != null) {
				switch (opcTagType) {
				case DOUBLE:
					try {
						if (opcNumberFormat != null && opcNumberFormat.length() > 0)
							currentValue.bind(Bindings.format(opcNumberFormat, opcTagValue));
						else
							currentValue.bind(Bindings.format("%.0f", opcTagValue));

					}
					catch (Exception ex) {
						// Unable to format as double
						currentValue.bind(Bindings.convert(opcTagValue));
					}
					break;

				case INTEGER:
					try {
						currentValue.bind(Bindings.format("%d", opcTagValue));
					}
					catch (Exception ex) {
						// Unable to format as integer
						currentValue.bind(Bindings.convert(opcTagValue));
					}
					break;

				default:
					currentValue.bind(Bindings.convert(opcTagValue));
					break;
				}
			}

			// Add click handler
			setOnMouseClicked(clickHandler);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private ChangeListener<String> currentValueListener = new ChangeListener<String>() {
		public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
			setText(newValue);
		}
	};

	private EventHandler<MouseEvent> clickHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent mouseEvent) {
			logger.debug("Clicked:" + opcEditable);
			if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
				if (mouseEvent.getClickCount() == 1) {
					if (opcEditable == true)
						showTagValueEditor();
				}
			}
		}
	};

	private void showTagValueEditor() {
		// TODO Port to Java 8u40 dialogs
//		Stage stage = (Stage)this.getScene().getWindow();
//		String str = Dialogs.showInputDialog(stage, "Prufuskilaboð...", "Masthead texti hér", "Titillinn", currentValue.get());
//		try {
//			OpcUtil.setTagValue(opcServer, opcSubscription, opcTagName, Double.valueOf(str));
//		}
//		catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		logger.debug(str);

	}

	public double getCurrentValue() {
		return Double.valueOf(this.currentValue.getValue());
	}

	public boolean getOpcEditable() {
		return opcEditable;
	}

	public void setOpcEditable(boolean t) {
		opcEditable = t;
	}

	public String getOpcNumberFormat() {
		return opcNumberFormat;
	}

	public void setOpcNumberFormat(String t) {
		opcNumberFormat = t;
	}

	@Override
	public String getOpcServer() {
		return opcServer;
	}

	@Override
	public void setOpcServer(String t) {
		opcServer = t;
	}

	@Override
	public String getOpcSubscription() {
		return opcSubscription;
	}

	@Override
	public void setOpcSubscription(String t) {
		opcSubscription = t;
	}

	@Override
	public String getOpcTagName() {
		return opcTagName;
	}

	@Override
	public void setOpcTagName(String t) {
		opcTagName = t;
	}

	@Override
	public TAGVALUETYPE getOpcTagType() {
		return opcTagType;
	}

	@Override
	public void setOpcTagType(TAGVALUETYPE t) {
		opcTagType = t;
		opcInit();
	}

}
