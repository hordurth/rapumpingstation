package com.roadauth.pumping.opcua.ui;

import com.roadauth.pumping.SpringContext;
import com.roadauth.pumping.opcua.service.OpcUAService;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class OpcTank extends AbstractOpcCanvasControl {

	private GraphicsContext gc;
	private DoubleProperty maxValueProperty = new SimpleDoubleProperty(100);
	private DoubleProperty currentValueProperty = new SimpleDoubleProperty(0);

	// hnitakerfi
	private double x1;
	private double x2;
	private double y1;
	private double y2;
	private double tankHeight;

	private ChangeListener<Number> sizeListener = new ChangeListener<Number>() {
		public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
			initIfReady();
		}
	};

	public OpcTank(double width, double height) {
		super(width, height);
		initTankur();
	}

	public OpcTank() {
		super();
		heightProperty().addListener(sizeListener);
		widthProperty().addListener(sizeListener);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void opcInit() {
		OpcUAService opcService = SpringContext.getBean(OpcUAService.class);

		opcTagValue = opcService.opcTagValue(opcTagName, opcTagType);
		if (opcTagValue != null)
			currentValueProperty().bind(opcTagValue);
	}

	public void initIfReady() {
		if (getWidth() > 0.0 && getWidth() > 0.0) {
			initTankur();
			heightProperty().removeListener(sizeListener);
			widthProperty().removeListener(sizeListener);
		}
	}

	@SuppressWarnings("unchecked")
	public void initTankur() {
		gc = getGraphicsContext2D();
		drawWaterHeight();
		currentValueProperty.addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				drawWaterHeight();
			}
		});
	}

	private void drawTank() {
		// setja x og y hnit fyrir ytri línur tanks
		x1 = 5.0;
		x2 = getWidth() - x1;
		y1 = 20.0;
		y2 = getHeight() - y1;
		// teikna ytri hluta tanks
		gc.setFill(Color.GRAY);
		gc.setStroke(Color.gray(0.2));
		gc.setLineWidth(5);
		gc.beginPath();
		gc.moveTo(x1, y1);
		gc.bezierCurveTo(x1, 0, x2, 0, x2, y1);
		gc.lineTo(x2, y2);
		gc.bezierCurveTo(x2, getHeight(), x1, getHeight(), x1, y2);
		gc.closePath();
		gc.stroke();
		gc.fill();

		// setja x og y hnit fyrir innri línur tanks
		x1 = 20.0;
		x2 = this.getWidth() - x1;
		y1 = 30.0;
		y2 = this.getHeight() - y1;
		// teikna innri hluta tanks
		gc.setFill(Color.DARKGRAY);
		gc.beginPath();
		gc.moveTo(x1, y1);
		gc.lineTo(x2, y1);
		gc.lineTo(x2, y2);
		gc.lineTo(x1, y2);
		gc.closePath();
		gc.stroke();
		gc.fill();
	}

	private void drawWaterHeight() {
		gc.clearRect(0, 0, getWidth(), getHeight());
		drawTank();
		// breyta efra y-hniti fyrir vatnshæð
		tankHeight = Math.abs(y2 - y1);
		double percentFilled = getCurrentValue() / getMaxValue();
		if (percentFilled > 1.0)
			percentFilled = 1.0;
		// teikna vatn
		if (percentFilled > 0.0) {
			y1 = y1 + tankHeight * (1.0 - percentFilled);
			gc.setFill(Color.DODGERBLUE);
			gc.beginPath();
			gc.moveTo(x1, y1);
			gc.lineTo(x2, y1);
			/*
			 * if(getCurrentValue()/getMaxValue()>0.98){ gc.lineTo(x2, y1); } else {
			 * gc.bezierCurveTo(x1, y1-10, x2, y1+10, x2, y1); //TODO: vökvaeffect - má
			 * fjarlægja }
			 */
			gc.lineTo(x2, y2);
			gc.lineTo(x1, y2);
			gc.closePath();
			gc.fill();
		}
	}

	public Double getMaxValue() {
		return maxValueProperty.get();
	}

	public void setMaxValue(Double value) {
		maxValueProperty.set(value);
	}

	public DoubleProperty maxValueProperty() {
		return maxValueProperty;
	}

	public Double getCurrentValue() {
		return currentValueProperty.get();
	}

	public void setCurrentValue(Double value) {
		if (value.doubleValue() < maxValueProperty.get())
			currentValueProperty.set(value);
		else
			currentValueProperty.set(maxValueProperty.get());
	}

	public DoubleProperty currentValueProperty() {
		return currentValueProperty;
	}

}
