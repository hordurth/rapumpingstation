package com.roadauth.pumping.opcua.ui;

import com.roadauth.pumping.SpringContext;
import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.ProgressBar;

public class OpcProgressBar extends ProgressBar implements IOpcControl {

	protected String opcServer = "";
	protected String opcSubscription = "";
	protected String opcTagName = "";
	protected TAGVALUETYPE opcTagType = TAGVALUETYPE.NONE;

	protected ObservableValue opcTagValue = null;

	// TODO Figure out from where we can call opcInit, maybe from a controller for
	// this custom control?

	@Override
	public void opcInit() {
		OpcUAService opcService = SpringContext.getBean(OpcUAService.class);

		opcTagValue = opcService.opcTagValue(opcTagName, opcTagType);
		if (opcTagValue != null)
			progressProperty().bind(opcTagValue);
	}

	@Override
	public String getOpcServer() {
		return opcServer;
	}

	@Override
	public void setOpcServer(String t) {
		opcServer = t;
	}

	@Override
	public String getOpcSubscription() {
		return opcSubscription;
	}

	@Override
	public void setOpcSubscription(String t) {
		opcSubscription = t;
	}

	@Override
	public String getOpcTagName() {
		return opcTagName;
	}

	@Override
	public void setOpcTagName(String t) {
		opcTagName = t;
	}

	@Override
	public TAGVALUETYPE getOpcTagType() {
		return opcTagType;
	}

	@Override
	public void setOpcTagType(TAGVALUETYPE t) {
		opcTagType = t;
	}

}
