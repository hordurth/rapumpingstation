package com.roadauth.pumping.opcua.ui;

import com.roadauth.pumping.SpringContext;
import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class OpcPump extends AbstractOpcCanvasControl {

	public static Integer OFF = 0;
	public static Integer FWD = 1;
	public static Integer REV = 2;
	public static Integer ALARM = 3;
	
	private Color colorOff = Color.gray(0.2);
	private Color colorOn = Color.GREEN;
	private Color colorAlert = Color.RED;

	protected String opcTagNameIsOff = "";
	protected String opcTagNameIsFwd = "";
	protected String opcTagNameIsRev = "";
	protected String opcTagNameIsAlarm = "";
	protected String opcTagNameTurnOn = "";
	protected String opcTagNameTurnOff = "";

	protected ObservableValue<Boolean> opcTagValueOff = null;
	protected ObservableValue<Boolean> opcTagValueFwd = null;
	protected ObservableValue<Boolean> opcTagValueRev = null;
	protected ObservableValue<Boolean> opcTagValueAlarm = null;
	
	//hnitakerfi
	private double x1;
	private double x2;
	private double x3;
	private double x4;
	private double y1;
	private double y2;
	private double y3;
	private double y4;

	private GraphicsContext gc;
	private IntegerProperty stateProperty = new SimpleIntegerProperty(OFF);
	
	private ChangeListener<Number> sizeListener = new ChangeListener<Number>() {
		public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
			initIfReady();
		}
	};

	private ChangeListener<Boolean> opcTagValueChangeListener = new ChangeListener<Boolean>() {
		public void changed(ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) {
			setStatus();
		}
	};

	public OpcPump(double width, double height){
		super(width, height);
		initDaela();
	}

	public OpcPump() {
		super();
		heightProperty().addListener(sizeListener);
		widthProperty().addListener(sizeListener);
	}
	
	private void setStatus() {
		boolean valueOff = (opcTagValueOff != null ? opcTagValueOff.getValue() : false);
		boolean valueFwd = (opcTagValueFwd != null ? opcTagValueFwd.getValue() : false);
		boolean valueRev = (opcTagValueRev != null ? opcTagValueRev.getValue() : false);
		boolean valueAlarm = (opcTagValueAlarm != null ? opcTagValueAlarm.getValue() : false);
		
		if (valueOff) stateProperty.set(OFF);
		else if (valueFwd) stateProperty.set(FWD);
		else if (valueRev) stateProperty.set(REV);
		else if (valueAlarm) stateProperty.set(ALARM);
	}
	
	public String getOpcTagNameIsOff() { return opcTagNameIsOff; }
	public void setOpcTagNameIsOff(String t) { opcTagNameIsOff = t; }

	public String getOpcTagNameIsFwd() { return opcTagNameIsFwd; }
	public void setOpcTagNameIsFwd(String t) { opcTagNameIsFwd = t; }

	public String getOpcTagNameIsRev() { return opcTagNameIsRev; }
	public void setOpcTagNameIsRev(String t) { opcTagNameIsRev = t; }

	public String getOpcTagNameIsAlarm() { return opcTagNameIsAlarm; }
	public void setOpcTagNameIsAlarm(String t) { opcTagNameIsAlarm = t; }

	public String getOpcTagNameTurnOff() { return opcTagNameTurnOff; }
	public void setOpcTagNameTurnOff(String t) { opcTagNameTurnOff = t; }

	public String getOpcTagNameTurnOn() { return opcTagNameTurnOn; }
	public void setOpcTagNameTurnOn(String t) { opcTagNameTurnOn = t; }

	@SuppressWarnings("unchecked")
	@Override
	public void opcInit() {
		OpcUAService opcService = SpringContext.getBean(OpcUAService.class);
		
		if (opcTagNameIsOff != null && (opcTagNameIsFwd != null || opcTagNameIsRev != null) && opcTagType != TAGVALUETYPE.NONE) {
			opcTagValueOff = opcService.opcTagValue(opcTagNameIsOff, opcTagType);
			if (opcTagValueOff != null)
				opcTagValueOff.addListener(opcTagValueChangeListener);
			opcTagValueFwd = opcService.opcTagValue(opcTagNameIsFwd, opcTagType);
			if (opcTagValueFwd != null)
				opcTagValueFwd.addListener(opcTagValueChangeListener);
			opcTagValueRev = opcService.opcTagValue(opcTagNameIsRev, opcTagType);
			if (opcTagValueRev != null)
				opcTagValueRev.addListener(opcTagValueChangeListener);
			if (opcTagNameIsAlarm != null) {
				opcTagValueAlarm = opcService.opcTagValue(opcTagNameIsAlarm, opcTagType);
				if (opcTagValueAlarm != null)
					opcTagValueAlarm.addListener(opcTagValueChangeListener);
			}
		}
	}

	public void initIfReady() {
		if (getWidth() > 0.0 && getWidth() > 0.0) {
			initDaela();
			heightProperty().removeListener(sizeListener);
			widthProperty().removeListener(sizeListener);
		}
	}

	public void initDaela() {
		setStatus();
		gc = this.getGraphicsContext2D();
		draw();
		stateProperty.addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                updateStatus();    
            }
        });
	}
	
	public void turnOff() {
		OpcUAService opcService = SpringContext.getBean(OpcUAService.class);
		opcService.setTagValue(opcTagNameTurnOff, true);
	}
	
	public void turnOn() {
		OpcUAService opcService = SpringContext.getBean(OpcUAService.class);
		opcService.setTagValue(opcTagNameTurnOn, true);
	}
	
	private void draw() {
		x1 = 5.0;
		x2 = x1+(this.getWidth()-2*x1)/3;
		x3 = x2+(this.getWidth()-2*x1)/3;
		x4 = this.getWidth()-x1;
		y1 = 5.0;
		y2 = x1+(this.getHeight()-2*x1)/3;
		y3 = x2+(this.getHeight()-2*x1)/3;
		y4 = this.getHeight()-x1;
		double[] xPoly = {x2, x1, x3};
		double[] yPoly = {y2,y4,y4};
		
		gc.setFill(Color.gray(0.2));
		gc.setStroke(Color.gray(0.2));
		gc.setLineWidth(2);
		
		// Þríhyrndur fótur
		gc.fillPolygon(xPoly, yPoly, 3);

		// Dælan sjálf
		gc.setFill(Color.GRAY);
		gc.fillOval(x1, y1, (x3-x1), (y3-y1));
		gc.strokeOval(x1, y1, (x3-x1), (y3-y1));
		gc.beginPath();
		gc.moveTo(x2, y1);
		gc.lineTo(x4, y1);
		gc.lineTo(x4, y2);
		gc.lineTo(x3, y2);
		gc.fill();
		gc.stroke();
		gc.closePath();
		
		// Teikna miðju eins og status segir til um
		updateStatus();
	}
	
	private void updateStatus() {
		if (stateProperty.get() == FWD || stateProperty.get() == REV)
			drawCentre(colorOn);
		else if (stateProperty.get() == OFF)
			drawCentre(colorOff);
		else if (stateProperty.get() == ALARM)
			drawCentre(colorAlert);
	}

	private void drawCentre(Color color) {
		double xspacing = (x3-x1)/7;
		double yspacing = (y3-y1)/7;
		double x = x1+xspacing;
		double y = y1+yspacing;
		double width = x3-x1-2*xspacing;
		double height = y3-y1-2*yspacing;
		boolean isOff = opcTagValueOff != null && opcTagValueOff.getValue() == true;
		boolean isFwd = opcTagValueFwd != null && opcTagValueFwd.getValue() == true && !isOff;
		boolean isRev = opcTagValueRev != null && opcTagValueRev.getValue() == true && !isOff;
		
		gc.setFill(color);
		gc.fillOval(x, y, width, height);
		gc.strokeOval(x, y, width, height);
		
		// Stefnuör
		gc.setFill(Color.BLACK);
		gc.setStroke(Color.BLACK);
		if (isFwd) {
			gc.beginPath();
			gc.moveTo(x + (width / 2), y + 3);
			gc.lineTo(x + width - 3, y + (height / 2));
			gc.lineTo(x + (width / 2), y + height - 3);
			gc.lineTo(x + (width / 2), y + 3);
			gc.fill();
			gc.closePath();
		}
		else if (isRev) {
			gc.beginPath();
			gc.moveTo(x + (width / 2), y + 3);
			gc.lineTo(x + 3, y + (height / 2));
			gc.lineTo(x + (width / 2), y + height - 3);
			gc.lineTo(x + (width / 2), y + 3);
			gc.fill();
			gc.closePath();
		}
	}

	public IntegerProperty stateProperty() {
		return stateProperty;
	}

}
