package com.roadauth.pumping;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.RAPumpingFXApplication.StageReadyEvent;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

@Component
public class StageInitializer implements ApplicationListener<StageReadyEvent> {

	private ApplicationContext applicationContext;

	@Value("classpath:/com/roadauth/pumping/view/MainView.fxml")
	private Resource mainViewResource;

	public StageInitializer(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void onApplicationEvent(StageReadyEvent event) {
		FXMLLoader fxmlLoader;
		try {
			// Get the main view
			fxmlLoader = new FXMLLoader(mainViewResource.getURL());
			fxmlLoader.setControllerFactory(aClass -> applicationContext.getBean(aClass));
			Parent parent = fxmlLoader.load();

			// Create window
			Stage stage = event.getStage();
			stage.setScene(new Scene(parent));
			stage.setTitle("Vegagerðin - Dælustöðvar");

			// Maximize window size
			Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
			stage.setX(0);
			stage.setY(0);
			stage.setWidth(screenBounds.getWidth());
			stage.setHeight(screenBounds.getHeight());
			stage.show();
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}