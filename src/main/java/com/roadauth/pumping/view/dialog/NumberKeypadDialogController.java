package com.roadauth.pumping.view.dialog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.view.model.NumberKeypadDialogModel;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;

@Component
public class NumberKeypadDialogController {

	@Autowired
	private NumberKeypadDialogModel model;

	@FXML
	private AnchorPane rootView;

	@FXML
	private Label lblTalaOrginal;

	@FXML
	private Label lblTala;

	@FXML
	private Button btnHnappurKomma;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnHnappur8;

	@FXML
	private Button btnHnappur9;

	@FXML
	private Button btnHnappur6;

	@FXML
	private Button btnHnappur7;

	@FXML
	private Button btnHnappur4;

	@FXML
	private Button btnHnappur5;

	@FXML
	private Button btnHnappur2;

	@FXML
	private Button btnHnappur3;

	@FXML
	private Button btnHnappur0;

	@FXML
	private Button btnHnappur1;

	@FXML
	private Button btnHnappurC;

	@FXML
	private Button btnHaettaVid;

	private boolean hefurKommu;
	private int naestiKommustafur;

	public NumberKeypadDialogController() {
	}

	@FXML
	private void initialize() {
		hefurKommu = false;
		naestiKommustafur = 0;
		lblTala.textProperty().bind(model.getValueEnteredProperty().asString("%,01." + model.getDecimalDigits() + "f"));
		lblTalaOrginal.textProperty().bind(model.getValueNowProperty().asString("%,01." + model.getDecimalDigits() + "f"));
		btnHnappurKomma.disableProperty().set(model.getDecimalDigits() == 0);
	}

	@FXML
	private void handleHaettaVid(ActionEvent event) {
		model.setValueNew(null);
		Scene sc = rootView.getScene();
		Window wi = sc.getWindow();
		((Stage) rootView.getScene().getWindow()).close();
	}

	@FXML
	private void handleILagi(ActionEvent event) {
		model.setValueNew(model.getValueEntered());
		((Stage) rootView.getScene().getWindow()).close();
	}

	@FXML
	private void handleToluhnappur(ActionEvent event) {
		Button hnappur = (Button) event.getSource();
		if (hnappur != null) {
			String adgerd = hnappur.getText();

			if (adgerd.equals(",") && !hefurKommu) {
				// Bæta við kommu ef tala inniheldur ekki kommu nú þegar
				hefurKommu = true;
				naestiKommustafur = 0;
			}
			else if (adgerd.equals("C")) {
				// Hreinsa ==> tala verður 0
				model.setValueEntered(0.0);
				hefurKommu = false;
				naestiKommustafur = 0;
			}
			else {
				String talan = String.valueOf(model.getValueEntered());
				String[] partar = talan.split("\\.");
				String heiltala = partar[0];
				String brot = partar[1];

				// Tölustafur, bæta við aftast í heiltöluna eða á réttan stað í brotið eftir kommustatusnum
				if (!hefurKommu) {
					if (heiltala.equals("0"))
						heiltala = adgerd;
					else
						heiltala += adgerd;
				}
				else if (naestiKommustafur < model.getDecimalDigits()) {
					if (naestiKommustafur > 0)
						brot = brot.substring(0, naestiKommustafur) + adgerd;
					else
						brot = adgerd;
					naestiKommustafur++;
				}

				talan = heiltala + (hefurKommu ? "." + brot : "");
				double dTalan = Double.valueOf(talan);
				if (dTalan <= model.getMaxEnteredValue())
					model.setValueEntered(dTalan);
			}
		}
	}

}
