package com.roadauth.pumping.view.dialog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.utils.RAPumpingUtils;
import com.roadauth.pumping.view.model.NumberKeypadDialogModel;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

@Component
public class NumberKeypadDialog {

	@Autowired
	private RAPumpingUtils utils;
	
	@Autowired
	private NumberKeypadDialogModel model;

	@Value("classpath:/com/roadauth/pumping/dialog/NumberKeypadDialog.fxml")
	private Resource myFxml;

	public Double showAndWait(Double valueNow, int decimalDigits, double maxEnteredValue) {
		model.setValueNow(valueNow);
		model.setValueEntered(0.0);
		model.setDecimalDigits(decimalDigits);
		model.setMaxEnteredValue(maxEnteredValue);
		
		Stage stage = new Stage(StageStyle.DECORATED);
		stage.initModality(Modality.WINDOW_MODAL);
		
		Parent view = utils.loadFxml(myFxml);
		Scene window = new Scene(view);
		stage.setScene(window);
		stage.showAndWait();
		
		return model.getValueNew();
	}

}
