package com.roadauth.pumping.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.opcua.data.model.OpcAlarm;
import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.ui.OpcLabel;
import com.roadauth.pumping.opcua.ui.OpcPump;
import com.roadauth.pumping.opcua.ui.OpcTank;
import com.roadauth.pumping.preferences.service.PreferencesService;
import com.roadauth.pumping.utils.RAPumpingUtils;
import com.roadauth.pumping.view.dialog.NumberKeypadDialog;

import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

@Component
public class TanksController {

	private static final Logger logger = LoggerFactory.getLogger(TanksController.class);

	@Autowired
	private RAPumpingUtils utils;

	@Autowired
	OpcUAService opcService;

	@Autowired
	PreferencesService prefsService;

	@FXML
	private OpcTank tankTar; // Bik

	@FXML
	private OpcLabel lblTarAmount;

	@FXML
	private OpcPump pumpTar;

	@FXML
	private OpcTank tankFastener; // Wetfix

	@FXML
	private OpcLabel lblFastenerAmount;

	@FXML
	private OpcPump pumpFastener;

	@FXML
	private OpcTank tankThinner; // Lifolia

	@FXML
	private OpcLabel lblThinnerAmount;

	@FXML
	private OpcPump pumpThinner;

	@FXML
	private Button btnResetFailureAlarm; // btnDIGEndursetjaBilun

	@FXML
	private TableView<OpcAlarm> tblAlarms;

	private ObservableList<OpcAlarm> alarms;

	public TanksController() {
	}

	@FXML
	public void initialize() {
		logger.info("Initializing TanksController");

		alarms = opcService.getActiveAlarmsList();
		btnResetFailureAlarm.setDisable(alarms.size() == 0);
		alarms.addListener(new ListChangeListener<OpcAlarm>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends OpcAlarm> c) {
				logger.info("Refreshing alarms tableview");
				while (c.next()) {
					if (c.wasUpdated()) {
						OpcAlarm alarm = c.getList().get(c.getFrom());
						if (alarm != null) {
							int idx = alarms.indexOf(alarm);
							if (idx > -1) {
								logger.info("Alarm tag was updated: " + alarm.getSubscriptionTagNameShort() + ", status=" + alarm.getStatusProperty().getValue());
								alarms.set(idx, alarm);
							}
						}
					}
				}
				btnResetFailureAlarm.setDisable(alarms.size() == 0);
			}
		});
		tblAlarms.setItems(alarms);
	}

	@SuppressWarnings("unchecked")
	@FXML
	private void tarTankAmountClicked() {
		if (prefsService.tarTankAmountIsEditable()) {
			ObservableValue<Double> nuna = opcService.opcTagValue(lblTarAmount.getOpcTagName(), OpcUAService.TAGVALUETYPE.DOUBLE);

			NumberKeypadDialog nkd = utils.getApplicationContext().getBean(NumberKeypadDialog.class);
			Double newValue = nkd.showAndWait(nuna.getValue().doubleValue(), 0, 999999.0);
			if (newValue != null)
				opcService.setTagValue(lblTarAmount.getOpcTagName(), newValue.floatValue());
		}
	}

	@SuppressWarnings("unchecked")
	@FXML
	protected void fastenerTankAmountClicked() {
		if (prefsService.fastenerTankAmountIsEditable()) {
			ObservableValue<Double> nuna = opcService.opcTagValue(lblFastenerAmount.getOpcTagName(), OpcUAService.TAGVALUETYPE.DOUBLE);

			NumberKeypadDialog nkd = utils.getApplicationContext().getBean(NumberKeypadDialog.class);
			Double newValue = nkd.showAndWait(nuna.getValue().doubleValue(), 0, 999999.0);
			if (newValue != null)
				opcService.setTagValue(lblFastenerAmount.getOpcTagName(), newValue.floatValue());
		}
	}

	@SuppressWarnings("unchecked")
	@FXML
	protected void thinnerTankAmountClicked() {
		if (prefsService.thinnerTankAmountIsEditable()) {
			ObservableValue<Double> nuna = opcService.opcTagValue(lblThinnerAmount.getOpcTagName(), OpcUAService.TAGVALUETYPE.DOUBLE);

			NumberKeypadDialog nkd = utils.getApplicationContext().getBean(NumberKeypadDialog.class);
			Double newValue = nkd.showAndWait(nuna.getValue().doubleValue(), 0, 999999.0);
			if (newValue != null)
				opcService.setTagValue(lblThinnerAmount.getOpcTagName(), newValue.floatValue());
		}
	}

	@FXML
	private void resetFailureAlarmBtnClick(ActionEvent event) {
		logger.info("handleDIGEndursetjaBilun");
		opcService.setTagValue("Vegagerdin.Device1.SkipanirFraSkjakerfi.Daeling_Endurs_wr", true);

		for (int i = alarms.size() - 1; i >= 0; i--)
			opcService.acknowledgeAlarm(alarms.get(i));
	}

	@FXML
	private void resetSoundAlarmBtnClick(ActionEvent event) {
		logger.info("handleDIGEndursetjaHljodgjafa");
		opcService.setTagValue("Vegagerdin.Device1.SkipanirFraSkjakerfi.Hljodgjafi_Af_wr", true);
	}

	@FXML
	private void alarmTableClicked(MouseEvent mouseEvent) {
		if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
			if (mouseEvent.getClickCount() == 1) {
				OpcAlarm alarm = tblAlarms.getSelectionModel().getSelectedItem();
				if (alarm != null)
					opcService.acknowledgeAlarm(alarm);
			}
		}
	}

}
