package com.roadauth.pumping.view.model;

import org.springframework.stereotype.Component;

import javafx.beans.property.SimpleDoubleProperty;

@Component
public class NumberKeypadDialogModel {

	private SimpleDoubleProperty valueNow = new SimpleDoubleProperty(0.0);
	private SimpleDoubleProperty valueEntered = new SimpleDoubleProperty(0.0);
	private Double valueNew = null;
	private int decimalDigits = 0;
	private double maxEnteredValue = 0.0;

	public double getValueNow() {
		return valueNow.get();
	}

	public SimpleDoubleProperty getValueNowProperty() {
		return valueNow;
	}

	public void setValueNow(double valueNow) {
		this.valueNow.set(valueNow);
	}

	public double getValueEntered() {
		return valueEntered.get();
	}

	public SimpleDoubleProperty getValueEnteredProperty() {
		return valueEntered;
	}

	public void setValueEntered(double valueEntered) {
		this.valueEntered.set(valueEntered);
	}

	public Double getValueNew() {
		return valueNew;
	}

	public void setValueNew(Double valueNew) {
		this.valueNew = valueNew;
	}

	public int getDecimalDigits() {
		return decimalDigits;
	}

	public void setDecimalDigits(int decimalDigits) {
		this.decimalDigits = decimalDigits;
	}

	public double getMaxEnteredValue() {
		return maxEnteredValue;
	}

	public void setMaxEnteredValue(double maxEnteredValue) {
		this.maxEnteredValue = maxEnteredValue;
	}

}
