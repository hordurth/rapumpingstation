package com.roadauth.pumping.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;
import com.roadauth.pumping.utils.RAPumpingUtils;
import com.roadauth.pumping.view.dialog.NumberKeypadDialog;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

@Component
public class TarTankHeatingController {

	@Autowired
	private RAPumpingUtils utils;

	@Autowired
	private OpcUAService opcService;

	@FXML
	private Label lblOskgildi;

	@FXML
	private Label lblRaungildi;

	@FXML
	private Label lblHitamaelir;

	@FXML
	private Label lblElement1;

	@FXML
	private Label lblElement2;

	@FXML
	private Label lblElement3;

	@FXML
	private Label lblElement4;

	@FXML
	private Button btnBreytaOskgildi;

	@FXML
	private Button btnStart;

	@FXML
	private Button btnStop;

	private ObservableValue<Double> oskgildiTjoruhita;
	private ObservableValue<Float> raungildiTjoruhita;
	private ObservableValue<Float> maeligildiTjoruhita;
	private ObservableValue<Boolean> hitaelement1;
	private ObservableValue<Boolean> hitaelement2;
	private ObservableValue<Boolean> hitaelement3;
	private ObservableValue<Boolean> hitaelement4;

	protected SimpleBooleanProperty isRunning = new SimpleBooleanProperty(false);

	public TarTankHeatingController() {
	}

	@SuppressWarnings("unchecked")
	@FXML
	void initialize() {
		oskgildiTjoruhita = opcService.opcTagValue("Vegagerdin.Device1.SkipanirFraSkjakerfi.TT31_2_SP_wr", OpcUAService.TAGVALUETYPE.DOUBLE);
		lblOskgildi.textProperty().bind(Bindings.format("%,01.0f", oskgildiTjoruhita));

		raungildiTjoruhita = opcService.opcTagValue("Vegagerdin.Device1.StodurTilSkjakerfis.TT31_2_Y",
				OpcUAService.TAGVALUETYPE.DOUBLE);
		lblRaungildi.textProperty().bind(Bindings.format("%,01.0f", raungildiTjoruhita));

		maeligildiTjoruhita = opcService.opcTagValue("Vegagerdin.Device1.StodurTilSkjakerfis.TT31_1_Y",
				OpcUAService.TAGVALUETYPE.DOUBLE);
		lblHitamaelir.textProperty().bind(Bindings.format("%,01.0f", maeligildiTjoruhita));

		hitaelement1 = opcService.opcTagValue("Vegagerdin.Device1.StodurTilSkjakerfis.E1_sk_rd",
				OpcUAService.TAGVALUETYPE.BOOLEAN);
		hitaelement1.addListener(elementChangeListener);
		lblElement1.setText(hitaelement1.getValue() ? "Á" : "Af");
		hitaelement2 = opcService.opcTagValue("Vegagerdin.Device1.StodurTilSkjakerfis.E2_sk_rd",
				OpcUAService.TAGVALUETYPE.BOOLEAN);
		hitaelement2.addListener(elementChangeListener);
		lblElement2.setText(hitaelement2.getValue() ? "Á" : "Af");
		hitaelement3 = opcService.opcTagValue("Vegagerdin.Device1.StodurTilSkjakerfis.E3_sk_rd",
				OpcUAService.TAGVALUETYPE.BOOLEAN);
		hitaelement3.addListener(elementChangeListener);
		lblElement3.setText(hitaelement3.getValue() ? "Á" : "Af");
		hitaelement4 = opcService.opcTagValue("Vegagerdin.Device1.StodurTilSkjakerfis.E4_sk_rd",
				OpcUAService.TAGVALUETYPE.BOOLEAN);
		hitaelement4.addListener(elementChangeListener);
		lblElement4.setText(hitaelement4.getValue() ? "Á" : "Af");

		btnStop.disableProperty().bind(isRunning.not());
		btnStart.disableProperty().bind(isRunning);

		ObservableValue<Boolean> runningTag = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION,
				"Vegagerdin.Device1.StodurTilSkjakerfis.HitiTank_IGangi_rd", TAGVALUETYPE.BOOLEAN);
		isRunning.bind(runningTag);
	}

	private ChangeListener<Boolean> elementChangeListener = new ChangeListener<Boolean>() {
		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			Label lbl = null;
			if (observable == hitaelement1)
				lbl = lblElement1;
			else if (observable == hitaelement2)
				lbl = lblElement2;
			else if (observable == hitaelement3)
				lbl = lblElement3;
			else if (observable == hitaelement4)
				lbl = lblElement4;
			if (lbl != null)
				lbl.setText(newValue != null && newValue == true ? "Á" : "Af");
		}
	};

	@FXML
	private void changeTargetValueClicked(ActionEvent event) {
		NumberKeypadDialog nkd = utils.getApplicationContext().getBean(NumberKeypadDialog.class);
		Double newValue = nkd.showAndWait(oskgildiTjoruhita.getValue().doubleValue(), 0, 200.0);
		if (newValue != null) {
			opcService.setTagValue("Vegagerdin.Device1.SkipanirFraSkjakerfi.TT31_2_SP_wr", Float.valueOf(newValue.floatValue()));
		}
	}

	@FXML
	private void startHeatingClicked(ActionEvent event) {
		opcService.setTagValue("Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiTank_Start_wr", true);
	}

	@FXML
	private void stopHeatingClicked(ActionEvent event) {
		opcService.setTagValue("Vegagerdin.Device1.SkipanirFraSkjakerfi.HitiTank_Stopp_wr", true);
	}

}
