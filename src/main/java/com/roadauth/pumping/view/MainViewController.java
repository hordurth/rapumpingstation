package com.roadauth.pumping.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.utils.RAPumpingUtils;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.TitledPane;

@Component
public class MainViewController {

	private static final Logger logger = LoggerFactory.getLogger(MainViewController.class);

	@Autowired
	private RAPumpingUtils utils;

	@Value("classpath:/com/roadauth/pumping/view/Tanks.fxml")
	private Resource tanksViewResource;

	@Value("classpath:/com/roadauth/pumping/view/Pumpings.fxml")
	private Resource pumpingsViewResource;

	@Value("classpath:/com/roadauth/pumping/view/HeatingHotOil.fxml")
	private Resource heatingHotOilViewResource;

	@Value("classpath:/com/roadauth/pumping/view/HeatingPipeline.fxml")
	private Resource heatingPipelineViewResource;

	@Value("classpath:/com/roadauth/pumping/view/TarTankHeating.fxml")
	private Resource tarTankHeatingViewResource;

	@FXML
	TitledPane tpTanks;

	@FXML
	TitledPane tpPumpings;

	@FXML
	TitledPane tpHeatingHotOil;

	@FXML
	TitledPane tpHeatingPipeline;

	@FXML
	TitledPane tpTarTankHeating;

	@FXML
	public void initialize() {
		logger.debug("MainViewController initialize");
		
		Parent view;
		
		// Add the tanks view
		view = utils.loadFxml(tanksViewResource);
		if (view != null)
			tpTanks.setContent(view);

		// Add the pumpings view
		view = utils.loadFxml(pumpingsViewResource);
		if (view != null)
			tpPumpings.setContent(view);

		// Add the heating hot oil view
		view = utils.loadFxml(heatingHotOilViewResource);
		if (view != null)
			tpHeatingHotOil.setContent(view);

		// Add the heating pipeline view
		view = utils.loadFxml(heatingPipelineViewResource);
		if (view != null)
			tpHeatingPipeline.setContent(view);

		// Add the tar tank heating view
		view = utils.loadFxml(tarTankHeatingViewResource);
		if (view != null)
			tpTarTankHeating.setContent(view);
	}

}
