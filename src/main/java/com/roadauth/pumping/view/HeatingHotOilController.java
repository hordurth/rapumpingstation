package com.roadauth.pumping.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roadauth.pumping.opcua.service.OpcUAService;
import com.roadauth.pumping.opcua.service.OpcUAService.TAGVALUETYPE;
import com.roadauth.pumping.opcua.ui.OpcPump;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

@Component
public class HeatingHotOilController {

	private static final Logger logger = LoggerFactory.getLogger(HeatingHotOilController.class);

	@Autowired
	protected OpcUAService opcService;

	@FXML
	protected Button btnStart;

	@FXML
	protected Button btnStop;

	@FXML
	protected OpcPump pump;

	protected SimpleBooleanProperty isRunning = new SimpleBooleanProperty(false);

	public HeatingHotOilController() {
	}

	@FXML
	@SuppressWarnings("unchecked")
	public void initialize() {
		btnStop.disableProperty().bind(isRunning.not());
		btnStart.disableProperty().bind(isRunning);

		ObservableValue<Boolean> runningTag = opcService.getSubscriptionTagProperty(OpcUAService.OPC_SERVER, OpcUAService.OPC_SUBSCRIPTION,
				pump.getOpcTagNameIsFwd(), TAGVALUETYPE.BOOLEAN);
		isRunning.bind(runningTag);
	}

	@FXML
	private void startHeatingClicked(ActionEvent event) {
		try {
			// Start the pump
			logger.info("HeatingOil start button clicked");
			pump.turnOn();
		}
		catch (Exception ex) {
			logger.error("Error starting HeatingHotOil pump", ex);
		}
	}

	@FXML
	private void stopHeatingClicked(ActionEvent event) {
		try {
			// Stop the pump
			logger.info("HeatingOil stop button clicked");
			pump.turnOff();
		}
		catch (Exception ex) {
			logger.error("Error stopping HeatingHotOil pump", ex);
		}
	}

}
