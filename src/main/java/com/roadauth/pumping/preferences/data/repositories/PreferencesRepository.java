package com.roadauth.pumping.preferences.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roadauth.pumping.preferences.data.model.Preference;

@Repository
public interface PreferencesRepository extends JpaRepository<Preference, String> {

}
