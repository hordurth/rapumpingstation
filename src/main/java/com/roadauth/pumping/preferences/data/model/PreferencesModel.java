package com.roadauth.pumping.preferences.data.model;

import java.util.HashMap;

import org.springframework.stereotype.Component;

@Component
public class PreferencesModel {

	private HashMap<String, String> preferences = new HashMap<String, String>();

	/**
	 * Adds a preferences item to the map.
	 * 
	 * @param name The pref key
	 * @param value the value for the key
	 */
	public void addPreference(String name, String value) {
		preferences.put(name, value);
	}

	/**
	 * Returns the preference key value or an empty string if the key does not exist.
	 * 
	 * @param name The pref key
	 * @return value The value for the key, or null if the key was not found
	 */
	public String getPreference(String name) {
		return preferences.get(name);
	}

}
