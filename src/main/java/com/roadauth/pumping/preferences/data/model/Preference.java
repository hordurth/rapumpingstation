package com.roadauth.pumping.preferences.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "preferences")
public class Preference {

	@Column(name = "pref_name")
	@Id
	private String prefName;

	@Column(name = "pref_value")
	private String prefValue;

	public String getPrefName() {
		return prefName;
	}

	public String getPrefValue() {
		return prefValue;
	}

	public void setPrefValue(String prefValue) {
		this.prefValue = prefValue;
	}

}
