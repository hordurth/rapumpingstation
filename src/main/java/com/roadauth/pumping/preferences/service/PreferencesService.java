package com.roadauth.pumping.preferences.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roadauth.pumping.preferences.data.model.Preference;
import com.roadauth.pumping.preferences.data.model.PreferencesModel;
import com.roadauth.pumping.preferences.data.repositories.PreferencesRepository;

@Service
public class PreferencesService {

	@Autowired
	private PreferencesModel prefsModel;

	@Autowired
	private PreferencesRepository prefsRepo;

	private static final String VG_TANK_ASFALT_AMOUNTEDITABLE = "vg.tank.asfalt.amounteditable";
	private static final String VG_TANK_LIFOLIA_AMOUNTEDITABLE = "vg.tank.lifolia.amounteditable";
	private static final String VG_TANK_WETFIX_AMOUNTEDITABLE = "vg.tank.wetfix.amounteditable";

	@PostConstruct
	public void initPreferences() {
		List<Preference> prefsList = prefsRepo.findAll();
		for (Preference prefItem : prefsList)
			prefsModel.addPreference(prefItem.getPrefName(), prefItem.getPrefValue());
	}

	public String getPrefValueForName(String name) {
		return prefsModel.getPreference(name);
	}
	
	public boolean tarTankAmountIsEditable() {
		return getPrefValueForNameAsBoolean(VG_TANK_ASFALT_AMOUNTEDITABLE);
	}
	
	public boolean thinnerTankAmountIsEditable() {
		return getPrefValueForNameAsBoolean(VG_TANK_LIFOLIA_AMOUNTEDITABLE);
	}
	
	public boolean fastenerTankAmountIsEditable() {
		return getPrefValueForNameAsBoolean(VG_TANK_WETFIX_AMOUNTEDITABLE);
	}
	
	private boolean getPrefValueForNameAsBoolean(String name) {
		boolean result = false;
		String value = prefsModel.getPreference(name);
		if (value != null)
			result = value.equalsIgnoreCase("true");
		return result;
	}

}
