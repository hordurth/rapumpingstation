package com.roadauth.pumping;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import javafx.application.Application;

@SpringBootApplication
public class RAPumpingSBApplication {

	public static void main(String[] args) {
		// SpringApplication.run(RAPumpingSBApplication.class, args);
		Application.launch(RAPumpingFXApplication.class, args);
	}

}
